function x = pmpi(x)
    % PMPI Converts an angle to +-pi range
    while x < -pi
        x = x + 2*pi;
    end
    while x > pi
        x = x - 2*pi;
    end
end

