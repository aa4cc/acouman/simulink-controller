function x = pmpi_2(x)
    % PMPI_2 Converts an angle to +-pi/2 range
    while x < -pi/2
        x = x + pi;
    end
    while x > pi/2
        x = x - pi;
    end
end

