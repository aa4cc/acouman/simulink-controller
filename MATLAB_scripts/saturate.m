function u_sat = saturate(u,u_min,u_max)
% SATURATE Saturates output
%
% U_SAT = SATURATE(U, U_MIN, U_MAX) Takes input U and returns:
%  * U     if U_MIN < U < U_MAX
%  * U_MIN if U <= U_MIN
%  * U_MAX if U >= U_MAX
    u_sat = max(min(u, u_max), u_min);
end

