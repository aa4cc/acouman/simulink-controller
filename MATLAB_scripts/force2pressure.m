function [xP,yP,P] = force2pressure(position,force,P_const,P_offset,R)    
    F = sqrt(force'*force);
    P = P_const*F + P_offset;
    if F > 1e-15
        cosA = force(1)/F;
        sinA = force(2)/F;
        xP = position(1) - (R+0.0015)*cosA;
        yP = position(2) - (R+0.0015)*sinA;
    else
        xP = 0.075;
        yP = -0.075;
    end
end