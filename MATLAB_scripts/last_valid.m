function y = last_valid(u)
% LAST_VALID Outputs last valid value of input
%
% Y = LAST_VALID(U) Returns the latest value of U that is not NaN. If the
% function hasn't received valid U, it outputs zero.
    persistent valid_u
    if isempty(valid_u)
        valid_u = single(0);
    end
    
    if ~isnan(u)
        valid_u = u;
    end
    y = valid_u;
end

