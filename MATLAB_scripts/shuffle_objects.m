function [x_s, y_s] = shuffle_objects(x, y, last_x, last_y)
    N = int32(numel(x));
    unassigned = true(N, 1);
    
    x_s = zeros(N, 1, class(x));
    y_s = zeros(N, 1, class(y));
    
    for k = int32(1):N
        arg = int32(0);
        best = single(inf);
        for m = int32(1):N
            if unassigned(m)
                distance = (x(m) - last_x(k))^2 + (y(m) - last_y(k))^2;
                if distance < best
                    best = distance;
                    arg = m;
                end
            end
        end
        unassigned(arg) = false;
        x_s(k) = x(arg);
        y_s(k) = y(arg);
    end
end
