function abs_p = absolute_pressure(M,c,s)
    num_pp = uint32(size(M,2)/2);
    abs_p = zeros(num_pp,1,'single');
    for i = uint32(1):num_pp
        Mi = [M(:,2*i-1), M(:,2*i)];
%         P1 = Mi*Mi';
%         P2 = Mi*[0,-2;2,0]*Mi';
% 
%         abs_p(i) = c'*P1*c + s'*P1*s + c'*P2*s;

        abs_p(i) = c'*(Mi*(Mi'*c)) + s'*(Mi*(Mi'*s)) + c'*(Mi*([0,-2;2,0]*(Mi'*s)));
    end