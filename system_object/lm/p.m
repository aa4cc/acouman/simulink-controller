function M = p(x,y,z,tx,ty)
    P0 = single(5); % transducer power [Pa.m]
    k = single(732.733); % wavenumber [m^{-1}]
%     r = 0.005; % transducer radius [m]
    global directivity
    
    m = int32(numel(x));
    n = int32(numel(tx));
    M = zeros(n, 2*m, 'single');
    for i = 1:m % iterate over points
        for j = 1:n % iterate over transducers
            xr = x(i) - tx(j);
            yr = y(i) - ty(j);
            d = sqrt(xr^2 + yr^2 + z(i)^2);
            p = P0*linterp(sqrt(xr^2 + yr^2)/d, directivity) / d;
            phase_shift = k*d;
            M(j,2*i-1) = p*cos(phase_shift);
            M(j,2*i) = p*sin(phase_shift);
        end
    end
end