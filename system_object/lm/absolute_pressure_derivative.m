function [abs_p_x, abs_p_y] = absolute_pressure_derivative(M,Mx,My,c,s)
    num_pp = uint32(size(M,2)/2);
    abs_p_x = zeros(num_pp,1,'single');
    abs_p_y = zeros(num_pp,1,'single');
    for i = uint32(1):num_pp
        p = [M(:,2*i-1), M(:,2*i)];
        px = [Mx(:,2*i-1), Mx(:,2*i)];
        py = [My(:,2*i-1), My(:,2*i)];
        abs_p_x(i) = 2*( c'*(px*(p'*c)) + s'*(px*(p'*s)) + ...
            c'*(px*([0,-1;1,0]*(p'*s))) - s'*(px*([0,-1;1,0]*(p'*c))) );
        abs_p_y(i) = 2*( c'*(py*(p'*c)) + s'*(py*(p'*s)) + ...
            c'*(py*([0,-1;1,0]*(p'*s))) - s'*(py*([0,-1;1,0]*(p'*c))) );
    end
end
