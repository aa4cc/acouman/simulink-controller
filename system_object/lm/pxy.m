function [M, Mx, My] = pxy(x,y,z,tx,ty)
    P0 = single(5); % transducer power [Pa.m]
    k = single(732.733); % wavenumber [m^{-1}]
%     r = 0.005; % transducer radius [m]
    global directivity directivity_derivative
    
    m = int32(numel(x));
    n = int32(numel(tx));
    M = zeros(n, 2*m, 'single');
    Mx = zeros(n, 2*m, 'single');
    My = zeros(n, 2*m, 'single');
    for i = 1:m % iterate over points
        for j = 1:n % iterate over transducers
            xr = x(i) - tx(j);
            yr = y(i) - ty(j);
            d = sqrt(xr^2 + yr^2 + z(i)^2);
            rho = sqrt(xr^2 + yr^2);
            sinTheta = rho / d;
            [f_dir, f_dir_derivative] = linterp(sinTheta, directivity, directivity_derivative);
            p = P0*f_dir/d;
            pi = p*k/d;
            pr = P0*f_dir_derivative*z(i)^2/d^5 - p/d^2;
            %pxr = P0*(f_dir_derivative*xr*z(i)^2/d^5 - f_dir*xr/d^3);            
            pxr = xr*pr;
            %pxi = P0*f_dir*k*xr/d^2;            
            pxi = xr*pi;            
            %pyr = P0*(f_dir_derivative*yr*z(i)^2/d^5 - f_dir*yr/d^3);
            pyr = yr*pr;
            %pyi = P0*f_dir*k*yr/d^2;
            pyi = yr*pi;
            
            phase_shift = k*d;
            c = cos(phase_shift);
            s = sin(phase_shift);
            M(j,2*i-1) = p*c;
            M(j,2*i) = p*s;
            Mx(j,2*i-1) = pxr*c - pxi*s;
            Mx(j,2*i) = pxr*s + pxi*c;
            My(j,2*i-1) = pyr*c - pyi*s;
            My(j,2*i) = pyr*s + pyi*c;
        end
    end
end

