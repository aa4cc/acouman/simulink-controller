function [Jx, Jy] = pressure_derivative_jacobian(M,Mx,My,c,s)
    num_pp = int32(size(M,2)/2);
    D = int32(size(M,1));
    Jx = zeros(num_pp,D,'single');
    Jy = zeros(num_pp,D,'single');
    
    for i = int32(1):num_pp
        p = [M(:,2*i-1), M(:,2*i)];
        px = [Mx(:,2*i-1), Mx(:,2*i)];
        py = [My(:,2*i-1), My(:,2*i)];
        
        pc = p'*c;
        ps = p'*s;
        pxc = px'*c;
        pxs = px'*s;
        pyc = py'*c;
        pys = py'*s;
        
        Jx(i, :) = 2 * ( -s.*(px*(pc + [-ps(2);ps(1)]) ...
                            + p*(pxc + [-pxs(2);pxs(1)])) + ...
                         c.*(px*(ps - [-pc(2);pc(1)]) ...
                            + p*(pxs - [-pxc(2);pxc(1)])) );
                        
        Jy(i, :) = 2 * ( -s.*(py*(pc + [-ps(2);ps(1)]) ...
                            + p*(pyc + [-pys(2);pys(1)])) + ...
                         c.*(py*(ps - [-pc(2);pc(1)]) ...
                            + p*(pys - [-pyc(2);pyc(1)])) );
    end
end

