clear global
global sinAngle directivity directivity_derivative N

N = int32(1001); % number of interpolation points
k = single(732.733); % wavenumber [m^{-1}]
r = single(0.005); % transducer radius [m]
sinAngle = single(linspace(0,1,N));
directivity = 2*besselj(1, k*r*sinAngle)./(k*r*sinAngle);
directivity(1) = 1;
directivity_derivative = besselj(0, k*r*sinAngle)./sinAngle.^2 - 2*besselj(1, k*r*sinAngle)./(k*r*sinAngle.^3) ...
    - besselj(2, k*r*sinAngle)./sinAngle.^2;
directivity_derivative(1) = 0;
