function phases = LM_solve_pressure_dist(x, y, z, reqP)
    % This function considers 16x16 transducer array and the origin of the coordinate
    % system is placed to the center of the transducer array
    
%     if nargin < 5
%         visualize = false;
%     end

    % Check the dimensions of the arguments
    assert(numel(x) == numel(y) && numel(y) == numel(z) && numel(z) == numel(reqP), ...
        "The lenghts of x, y, z, and reqP must be the same");

    % Optimization
    t_maxc = 0.045; % maximum distance from the pressure point within which the transducers are considered in the optimization
    t_maxd = 0.065; % maximum distance from the pressure point within which the transducers are optimized over

    % Physical parameters
    t_d = coder.const(0.01); % Diameter of the transducer [mm]
    N_x = 16; % Number of transducers along x axis
    N_y = 16; % Number of transducers along y axis
    arraySize = 256;

    origin = [ -(N_x-1)/2*t_d;  -(N_y-1)/2*t_d]; % Position of the center of the transducer with index (1, 1) -- i.e. the bottom-left-most one

    % Number of pressure points
    N_p = numel(reqP);

    struct_prototype = struct( 'reqP', 0, ...
                'pos',  [0; 0; 0], ...
                'c',    zeros(0, 1, 'uint32'), ...% global indexes of the elements of ubar, y, z
                'd',    zeros(0, 1, 'uint32'), ...% indexes of the elements of ubar, y, z which are optimized over
                'ubar', zeros(0, 1), ...
                'y',    zeros(0, 1), ...
                'z',    zeros(0, 1), ...
                'M',    zeros(0, 2)); % Modulus of the transducer's model

    pp = repmat(struct_prototype, [N_p, 1]);
    coder.varsize('pp(:).c',    [arraySize, 1]);
    coder.varsize('pp(:).d',    [arraySize, 1]);
    coder.varsize('pp(:).ubar', [arraySize, 1]);
    coder.varsize('pp(:).y',    [arraySize, 1]);
    coder.varsize('pp(:).z',    [arraySize, 1]);
    coder.varsize('pp(:).M',    [arraySize, 2]);

    %% Initialize the optimization variables
    uPlusZ = zeros(arraySize, 1);
    N_used = zeros(arraySize, 1); % Each component of this matrix stores the number of presure points using the corresponding transducer
  
    %% Initialize the pp structures
    for i = 1:N_p
        pp(i).reqP = reqP(i); % required pressure
        pp(i).pos = [x(i); y(i); z(i)]; % position 

        % Determine the neighboring transducers
        [pp(i).c, pp(i).d] = nNeighbors(x(i), y(i));

        % Compute the power modulus of the used transducers and normalize
        % it
        pp(i).M = p_transInd(x(i), y(i), z(i), pp(i).c)./reqP(i);

        % Initiliaze all the remaining variables to zeros
        pp(i).ubar = zeros(numel(pp(i).c), 1);
        pp(i).y = zeros(numel(pp(i).c), 1);
        pp(i).z = zeros(numel(pp(i).c), 1); 
        
        % Increase by one the number of the counter for each used
        % transducer
        N_used(pp(i).c) = N_used(pp(i).c) + 1;
    end
    
    
%     if visualize
%         % Plot the used transducers    
%         clrs = colormap('lines');
%         figure(1)
%         clf
%         hold on
%         % Plot the transducers        
%         [it, jt] = ind2sub(1:arraySize);
%         transPos = transdcuerPosition(it, jt);
%         scatter(transPos(1,:), transPos(2,:), 'o', 'MarkerEdgeColor', [0.5, 0.5, 0.5])
% 
%         % Plot the position of the pressure point
%         scatter(x, y, 'x', 'LineWidth', 2);
%         for i = 1:N_p        
%             % Plot the circle within which the used transducers should be
%             % located
%             th = linspace(0, 2*pi, 50);
%             plot(x(i)+t_maxd*cos(th), y(i)+t_maxd*sin(th), 'Color', clrs(i,:));
% 
%             % Plot the used transducers
%             [it, jt] = ind2sub(pp(i).c');
%             transPos = transdcuerPosition(it, jt);
%             scatter(transPos(1, :), transPos(2, :), 20, clrs(i, :), 'filled', 'MarkerFaceAlpha', 0.5)
%         end    
%         hold off
%     end

    maxiter = 15;
    lambda = 10000;
    rho = 0.0001;
    for i=1:maxiter
        uPlusZ(:) = 0;
        % Update u_bar
        for k=1:N_p
            pp(k).ubar = pp(k).y - pp(k).z;
            c = cos(pp(k).ubar);
            s = sin(pp(k).ubar);
            
            % Compute the jacobian
            J = pressure_jacobian(pp(k).M, c, s, pp(k).d);

            R_squared = sum(J.^2) + 1/lambda;
            rhs = absolute_pressure(pp(k).M, c, s) - 1;
%             fprintf('Agnt #%d: %f \n', k, rhs);
            step = -J*(rhs/R_squared);
            
            pp(k).ubar(pp(k).d) = pp(k).ubar(pp(k).d) + step;
            
            uPlusZ(pp(k).c) = uPlusZ(pp(k).c) + pp(k).ubar + pp(k).z;
        end
        
        % Update y
        for k=1:N_p
            pp(k).y = uPlusZ(pp(k).c)./N_used(pp(k).c);
        end
                
        % Update z
        for k=1:N_p
            pp(k).z = pp(k).z + rho*(pp(k).ubar - pp(k).y);
        end
    end
    
    % Compute the phases
    phases = uPlusZ./N_used;
    
    function k = sub2ind(i, j)
         k = double(j) + (double(i) - 1).*N_y;
    end

    function [i, j] = ind2sub(k)
        vi = rem(k-1, N_y) + 1;
        i = double((k - vi)/N_y + 1);
        j = double(vi);
    end

    function pos = transdcuerPosition(i, j)
        % return a position of the (i,j)-th transducer
        pos = origin + [double(i-1)*t_d; double(j-1)*t_d];
    end

    function M = p_transInd(x, y, z, k)        
        [ti, tj] = ind2sub(k);
        
        P0 = 5; % transducer power [Pa.m]
        k = 732.733; % wavenumber [m^{-1}]
        r = 0.005; % transducer radius [m]
        n = numel(k);
        xr = repmat(x,n,1) - origin(1) - double(ti-1)*r*2;
        yr = repmat(y,n,1) - origin(2) - double(tj-1)*r*2;
        epsilon = 1e-6; % small error
        d = sqrt(xr.^2 + yr.^2 + repmat(z'.^2,n,1));
        sinTheta = sqrt(xr.^2 + yr.^2)./d + epsilon;    

        p = (2.*P0.*besselj(1, k.*r.*sinTheta).*exp(1j*k*d))./(d.*k.*r.*sinTheta);
        M = [real(p) imag(p)];
    end

    function J = pressure_jacobian(M,c,s,d)
        Mi1 = M(:,1);
        Mi2 = M(:,2);
            
        J = 2*( (-c(d).*Mi2(d)-s(d).*Mi1(d))*(Mi1'*c)  + (c(d).*Mi1(d)-s(d).*Mi2(d))*(Mi2'*c)) + ...
            2*( (c(d).*Mi1(d)-s(d).*Mi2(d))*(Mi1'*s)    +  (c(d).*Mi2(d)+s(d).*Mi1(d))*(Mi2'*s));
    end

    function [transList_c, transList_d] = nNeighbors(x, y)
        % Maximum number of transducers in one direction
        n_adjCoils = int32(ceil(t_maxc / t_d));

        % Pre-allocate the transList_c - list of global indexes of transducers
        % considered in the model of the pressure for the pressure point at (x,y)
        transList_c = zeros(256, 1, 'uint32');
        % Pre-allocate the transList_d - list of local indexes of transducers
        % considered in optimization problem for the pressure point at (x,y)
        transList_d = zeros(256, 1, 'uint32');

        % Find the indexes (i, j) of the transducer closest to the pressure point at (x, y)
        ic = int32(round((x - origin(1))/t_d) + 1);
        jc = int32(round((y - origin(2))/t_d) + 1);

        N_c = 0; % number of transducers in transList_c
        N_d = 0; % number of transducers in transList_d
        % Iterate over the transducers adjacent to the transducer
        for ii = (ic-n_adjCoils):(ic+n_adjCoils)
            for jj = (jc-n_adjCoils):(jc+n_adjCoils)
                % Check whether we are not out of bounds
                if ii < 1 || ii > N_x || jj < 1 || jj > N_y
                    continue;
                end

                % Check whether the transducer is within the specified
                % distance from the the pressure point. If it is, add the
                % transducer to the list.
                transPos_x = origin(1) + double(ii-1)*t_d;
                transPos_y = origin(2) + double(jj-1)*t_d;
                dist = (transPos_x - x)^2 + (transPos_y - y)^2;

                if dist <= t_maxc^2
                    N_c = N_c + 1;
                    transList_c(N_c, 1) = sub2ind(ii, jj);
                    
                    if dist <= t_maxd^2
                        N_d = N_d + 1;
                        transList_d(N_d, 1) = N_c;
                    end
                end
            end
        end

        transList_c = transList_c(1:N_c, :);
        transList_d = transList_d(1:N_d, :);
    end

end
