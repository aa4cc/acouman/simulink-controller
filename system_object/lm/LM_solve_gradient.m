function phases = LM_solve_gradient(x,y,z,reqPx,reqPy,tx,ty)
    %% Preparation
    array_size = int32(numel(tx));
%     phases = pi*randn(array_size^2,1); % initial guess
    phases = zeros(array_size,1,'single'); % initial guess
    % calculate acoustic pressure
    [M,Mx,My] = pxy(x,y,z,tx,ty);
    c = cos(phases);
    s = sin(phases);
    [Px,Py] = absolute_pressure_derivative(M,Mx,My,c,s);
    o = [Px;Py];
    obj_fit = [reqPx;reqPy];
    penalty = (obj_fit - o)' * (obj_fit - o);
    [Jx,Jy] = pressure_derivative_jacobian(M,Mx,My,c,s);
    J = [Jx;Jy];
    nump_pp = 2*numel(x);
    
    %% Levenberg-Marquardt solver
    mu = 1; % initial damping

%     fprintf('|Iteration|Penalty |Damping |LSCount|\n');
%     fprintf('|---------|--------|--------|-------|\n');
    for i = uint32(1:7) % number of iterations
        rhs = o - obj_fit;
        num_search = uint32(1);
        while true
            Abar = [J'; sqrt(mu)*eye(nump_pp)];
%             [~, R] = qr(Abar, 0);
            R = qr(Abar, 0);
            R = R(1:nump_pp, 1:nump_pp);
%             step = -J'*((R'*R)\rhs);
            tmp = linsolve(R', rhs, struct('LT', true));
            tmp = linsolve(R,tmp, struct('UT', true));
            step = -J'*tmp;
            new_phases = phases + step;
            new_c = cos(new_phases);
            new_s = sin(new_phases);
            [Px,Py] = absolute_pressure_derivative(M,Mx,My,new_c,new_s);
            new_o = [Px;Py];
            new_penalty = (obj_fit - new_o)' * (obj_fit - new_o);
            if new_penalty < penalty
                phases = new_phases;
                c = new_c;
                s = new_s;
                o = new_o;
                penalty = new_penalty;
%                 fprintf('|%9d|%8.2e|%8.2e|%7d|\n',i,penalty,mu,num_search);
                [Jx,Jy] = pressure_derivative_jacobian(M,Mx,My,c,s);
                J = [Jx;Jy];
                mu = mu/2;
%                 fprintf('Iteration %d\n', i);
%                 fprintf('Achieved gradient: %f\n', o);
%                 fprintf('Penalty/gradient ratio: %e\n', norm(penalty) / norm(J));
%                 fprintf('=========================================\n');
                break;
            end
            mu = mu*2;
            num_search = num_search + 1;
            if (num_search > 10)
%                 fprintf('LMsolve failed to converge on iteration %u\n',i);
               break;
            end
        end
        if (penalty / norm(J) < 1e-3) % penalty tolerance
            break;
        end
    end