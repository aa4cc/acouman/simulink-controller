classdef LMPressurePoints < matlab.System ...
        & coder.ExternalDependency ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % This block solves the pressure points optimization problem every
    % simulation step.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    
    
    properties(Nontunable)
        % Array Array size
        Array (1,1) ArraySize
        % opt_type Optimization solver
        opt_type (1,1) OptTypeEnum = OptTypeEnum.central_nearby_transducers;
    end
    
    properties(Nontunable, Logical)
%         % ActiveArea Use only nearby transducers for optimization
%         ActiveArea = false
    end

    properties(Nontunable, Access=private)
        % x-coordinates of transducer array
        tx
        % y-coordinates of transducer array
        ty
        % position to indices lookup
        array_indices
    end

    methods
        % Constructor
        function obj = LMPressurePoints(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj)
            switch get(obj, 'Array')
                case ArraySize.eight_by_eight
                    coordinates = single(linspace(-0.035, 0.035, 8));
                    [x,y] = meshgrid(coordinates, coordinates);
                    obj.tx = reshape(x,64,1);
                    obj.ty = reshape(y,64,1);
                    obj.array_indices = reshape(uint32(1:64),8,8);
                case ArraySize.sixteen_by_sixteen
                    x_l = single(linspace( 0.005, 0.075,8));
                    x_r = single(linspace(-0.005,-0.075,8));
                    y_u = single(linspace( 0.075, 0.005,8));
                    y_d = single(linspace(-0.005,-0.075,8));
                    [ty0,tx0] = meshgrid(y_d,x_l);
                    tx0 = reshape(tx0,64,1);
                    ty0 = reshape(ty0,64,1);
                    [ty1,tx1] = meshgrid(y_u,x_l);
                    tx1 = reshape(tx1,64,1);
                    ty1 = reshape(ty1,64,1);
                    [ty2,tx2] = meshgrid(y_d,x_r);
                    tx2 = reshape(tx2,64,1);
                    ty2 = reshape(ty2,64,1);
                    [ty3,tx3] = meshgrid(y_u,x_r);
                    tx3 = reshape(tx3,64,1);
                    ty3 = reshape(ty3,64,1);
                    obj.tx = [tx0;tx1;tx2;tx3];
                    obj.ty = [ty0;ty1;ty2;ty3];
                    subarray = flipud(reshape(uint32(1:64),8,8)');
                    obj.array_indices = [128+fliplr(subarray), subarray;
                                         192+fliplr(subarray), 64+subarray];
            end
        end
        
        function [phases, varargout] = stepImpl(obj,x,y,z,reqP)
            global directivity
            switch obj.opt_type
                case OptTypeEnum.central_all_transducers
                    phases = LM_solve_pressure(x,y,z,reqP,obj.tx,obj.ty);
                case OptTypeEnum.central_nearby_transducers
                    enables = uint8(zeros(256,1));
                    for i=1:uint32(numel(x))
                        x_aligned = min( max(uint32(floor((x(i)+0.05)*100)), 1), 9);
                        y_aligned = min( max(uint32(floor((y(i)+0.05)*100)), 1), 9);
                        active_area = reshape(obj.array_indices(y_aligned:y_aligned+7, x_aligned:x_aligned+7)', 64, 1);
                        enables(active_area) = 1;
                    end
                    %phases = LM_solve_pressure_enabled(x,y,z,reqP,obj.tx,obj.ty,enables); 
                    phases = zeros(256, 1, 'single');
                    phases(logical(enables)) = LM_solve_pressure(x,y,z,reqP,obj.tx(logical(enables)),obj.ty(logical(enables)));
                    varargout{1} = enables;
                otherwise
                    phases_unsorted = LM_solve_pressure_dist(double(x),double(y),double(z),double(reqP));
                    phases = zeros(256,1,'single');
                    enables = ones(256,1,'uint8');
                    for k = 1:256
                        ii = obj.array_indices(k);
                        phases(ii) = single(phases_unsorted(k));
                        if isnan(phases(ii))
                            phases(ii) = single(0);
                            enables(ii) = uint8(0);
                        end
                    end
                    varargout{1} = enables;
            end
        end
        
        function releaseImpl(obj)

        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 4;
        end
        
        function num = getNumOutputsImpl(obj)
            if obj.opt_type == OptTypeEnum.central_all_transducers
                num = 1;
            else
                num = 2;
            end
        end
        
        function flag = isInputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function flag = isInputFixedSizeImpl(~,~)
            flag = false;
        end
        
        function flag = isInputComplexityLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isOutputFixedSizeImpl(obj)
            if obj.opt_type == OptTypeEnum.central_all_transducers
                varargout{1} = true;
            else
                varargout{1} = true;
                varargout{2} = true;
            end
        end

        function names = getGlobalNamesImpl(~)
            % Return names of global variables defined in Data Store Memory
            % blocks
            names = {'sinAngle', 'directivity'};
        end
        
        function varargout = getOutputSizeImpl(obj)
            switch obj.Array
                case ArraySize.eight_by_eight
                    if obj.opt_type == OptTypeEnum.central_all_transducers
                        varargout = {64};
                    else
                        varargout = {64,64};
                    end
                case ArraySize.sixteen_by_sixteen
                    if obj.opt_type == OptTypeEnum.central_all_transducers
                        varargout = {256};
                    else
                        varargout = {256,256};
                    end
            end
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            if obj.opt_type == OptTypeEnum.central_all_transducers
                varargout = {'single'};
            else
                varargout = {'single','uint8'};
            end
        end
        
        function varargout = isOutputComplexImpl(obj)
            if obj.opt_type == OptTypeEnum.central_all_transducers
                varargout = {false};
            else
                varargout = {false,false};
            end
        end
        
        function validateInputsImpl(obj, in1, in2, in3, in4)
            if isempty(coder.target)
                % Run input validation only in Simulation                
                validateattributes(in1,{'numeric'},{'size',[nan,1]});
                validateattributes(in2,{'numeric'},{'size',[nan,1]});
                validateattributes(in3,{'numeric'},{'size',[nan,1]});
                validateattributes(in4,{'numeric'},{'size',[nan,1]});
            end
        end
        
        function validatePropertiesImpl(~)
        end
        
        function icon = getIconImpl(~)
            % Define a string as the icon for the System block in Simulink.
                icon = sprintf('Pressure points\noptimization');
            
        end

        function [name1,name2,name3,name4] = getInputNamesImpl(~)
            % Return input port names for System block
            name1 = 'x';
            name2 = 'y';
            name3 = 'z';
            name4 = 'Required pressure';
        end

        function varargout = getOutputNamesImpl(obj)
            % Return output port names for System block
            varargout{1} = 'Phases';
            if obj.opt_type ~= OptTypeEnum.central_all_transducers
                varargout{2} = 'Enables';
            end
        end
        
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', LMPressurePoints.getDescriptiveName());
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Pressure points';
        end
        
        function b = isSupportedContext(~)

        end
        
        function updateBuildInfo(~, ~)

        end
    end
end
