classdef ArraySize < Simulink.IntEnumType
    enumeration
        eight_by_eight (0)
        sixteen_by_sixteen (1)
    end
    methods (Static)
        function retVal = getDefaultValue()
            retVal = ArraySize.eight_by_eight;
        end
  end
end
