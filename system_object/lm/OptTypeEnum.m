classdef OptTypeEnum < int32
    enumeration
        central_all_transducers     (1)
        central_nearby_transducers  (2)
        distributed                 (3)
    end
end
