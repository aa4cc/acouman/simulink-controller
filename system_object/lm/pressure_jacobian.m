function J = pressure_jacobian(M,c,s)
    num_pp = int32(size(M,2)/2);
    D = int32(size(M,1));
    J = zeros(num_pp,D,'single');
    
    for i = int32(1):num_pp
%         Mi = [M(:,2*i-1), M(:,2*i)];
%         Ji = 2*(diag(c)*Mi*[0,1;-1,0] - diag(s)*Mi)*Mi'*c + 2*(diag(c)*Mi+diag(s)*Mi*[0,1;-1,0])*Mi'*s;

%         Mi = [M(:,2*i-1), M(:,2*i)];
%         Mic = [c.*M(:,2*i-1), c.*M(:,2*i)];
%         Mis = [s.*M(:,2*i-1), s.*M(:,2*i)];
%         
%         Ji = 2*(Mic*[0,1;-1,0] - Mis)*(Mi'*c) + 2*(Mic+Mis*[0,1;-1,0])*(Mi'*s);
        
        Mi1 = M(:,2*i-1);
        Mi2 = M(:,2*i);
        
%         Ji = 2*[-c.*Mi2-s.*Mi1, c.*Mi1-s.*Mi2]*([Mi1';Mi2']*c) + 2*([c.*Mi1-s.*Mi2, c.*Mi2+s.*Mi1])*([Mi1';Mi2']*s);
        Ji = 2*( (-c.*Mi2-s.*Mi1)*(Mi1'*c)  + (c.*Mi1-s.*Mi2)*(Mi2'*c)) + ...
             2*( (c.*Mi1-s.*Mi2)*(Mi1'*s)    +  (c.*Mi2+s.*Mi1)*(Mi2'*s));
        
        
        J(i,:) = Ji;
    end