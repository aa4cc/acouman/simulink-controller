function varargout = linterp(xq, varargin)
    % LINTERP Evenly spaced linear interpolation
    %
    % [Y1, Y2, ...] = LINTERP(XQ, V1, V2, ...) This function assumes that 
    % the values Vi came from evenly sampled points ranging from 0 to 1.
    N = numel(varargin{1});
    index = xq*(N-1) + 1;
    index_rounded = int32(round(index));
    
%     varargout = cell(numel(varargin),1);
    for k = 1:nargin-1
        varargout{k} = varargin{k}(index_rounded);
    end
end