classdef BallLQR < matlab.System & matlab.system.mixin.Propagates
    % BALLLQR This block calculates LQ-optimal state feedback for a floating ball
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    properties (Nontunable)
        % K Feedback gain
        K single = [0, 0];
        % SAT Output saturation
        sat single = 1;
        % K_R Repulsive force coefficient
        K_r single = 1;
    end
    
    properties (Nontunable, PositiveInteger)
        % NUM_OBJ Number of objects
        num_obj = 1;
    end
    
    properties (Nontunable, Logical)
        % VEL_REF Use velocity reference
        vel_ref = true;
        % SHOW_EN Show enable port
        show_en = true;
    end
    

    methods(Access = protected)
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants

        end

        function varargout = stepImpl(obj, varargin)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            %varargout = cell(1, 2*obj.num_obj);
            
            %%% Calculate matrix of relative distances
            x_rel = zeros(obj.num_obj,'single');
            y_rel = zeros(obj.num_obj,'single');
            dist = eye(obj.num_obj,'single');
            for k = 1:obj.num_obj
                x_state_k = varargin{4*k-3};
                y_state_k = varargin{4*k-1};
                for m = (k+1):obj.num_obj
                    x_state_m = varargin{4*m-3};
                    y_state_m = varargin{4*m-1};
                    
                    x_rel(m,k) = x_state_m(2) - x_state_k(2);
                    x_rel(k,m) = - x_rel(m,k);
                    y_rel(m,k) = y_state_m(2) - y_state_k(2);
                    y_rel(k,m) = - y_rel(m,k);
                    d = sqrt(x_rel(m,k)^2 + y_rel(m,k)^2) + 1;
                    dist(k,m) = d;
                    dist(m,k) = d;
                end
            end
            
            if ~obj.show_en
                enables = true(obj.num_obj, 1);
            else
                enables = varargin{end};
            end

            for k = 1:obj.num_obj
                F_x = single(0);
                F_y = single(0);
                
                if enables(k)
                    if obj.vel_ref
                        state_error_x = varargin{4*k-2} - varargin{4*k-3};
                        state_error_y = varargin{4*k} - varargin{4*k-1};
                    else
                        state_error_x = [0;varargin{4*k-2}] - varargin{4*k-3};
                        state_error_y = [0;varargin{4*k}] - varargin{4*k-1};
                    end
                    F_x = obj.K * state_error_x; 
                    F_y = obj.K * state_error_y;
                    
                    [F_x, F_y] = sat_2D(F_x, F_y, obj.sat);
                    
                    F_x = F_x + obj.K_r * sum(x_rel(:,k) ./ (dist(:,k).^3));
                    F_y = F_y + obj.K_r * sum(y_rel(:,k) ./ (dist(:,k).^3));
                end
                
                varargout{2*k - 1} = F_x;
                varargout{2*k} = F_y;
            end
        end

        function validateInputsImpl(obj,varargin)
            % Validate inputs to the step method at initialization
            for k = 0:(obj.num_obj-1)
                validateattributes(varargin{4*k+1}, {'numeric'}, {'size', [2,1]}, ...
                    'validate LQR inputs', sprintf('x-state %d', k+1))
                validateattributes(varargin{4*k+3}, {'numeric'}, {'size', [2,1]}, ...
                    'validate LQR inputs', sprintf('y-state %d', k+1))
                if obj.vel_ref
                    validateattributes(varargin{4*k+2}, {'numeric'}, {'size', [2,1]}, ...
                        'validate LQR inputs', sprintf('x-reference %d', k+1))
                    validateattributes(varargin{4*k+4}, {'numeric'}, {'size', [2,1]}, ...
                        'validate LQR inputs', sprintf('y-reference %d', k+1))
                else
                    validateattributes(varargin{4*k+2}, {'numeric'}, {'scalar'}, ...
                        'validate LQR inputs', sprintf('x-reference %d', k+1))
                    validateattributes(varargin{4*k+4}, {'numeric'}, {'scalar'}, ...
                        'validate LQR inputs', sprintf('y-reference %d', k+1))
                end
            end
            
            if obj.show_en
                validateattributes(varargin{4*(obj.num_obj-1) + 5}, {'logical'}, ...
                    {'vector', 'numel', obj.num_obj}, 'validate LQR inputs', 'enable')
            end
        end

        function num = getNumInputsImpl(obj)
            % Define total number of inputs for system with optional inputs
            if obj.show_en
                num = 4*obj.num_obj + 1;
            else
                num = 4*obj.num_obj;
            end
        end

        function num = getNumOutputsImpl(obj)
            % Define total number of outputs for system with optional
            % outputs
            num = obj.num_obj * 2;
        end 
        
        function varargout = getOutputSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,2*obj.num_obj);
            for k = 1:2*obj.num_obj
                varargout{k} = [1 1];
            end
        end
        
        function varargout = isOutputFixedSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,2*obj.num_obj);
            for k = 1:2*obj.num_obj
                varargout{k} = true;
            end
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            % Return size for each output port
            varargout = cell(1,2*obj.num_obj);
            for k = 1:2*obj.num_obj
                varargout{k} = 'single';
            end
        end
        
        function varargout = isOutputComplexImpl(obj)
            % Return size for each output port
            varargout = cell(1,2*obj.num_obj);
            for k = 1:2*obj.num_obj
                varargout{k} = false;
            end
        end
        
        function varargout = isOutputComplexityLockedImpl(obj)
            % Return size for each output port
            varargout = cell(1,2*obj.num_obj);
            for k = 1:2*obj.num_obj
                varargout{k} = true;
            end
        end

        function varargout = getInputNamesImpl(obj)
            % Return input port names for System block
            varargout = cell(1, obj.getNumInputsImpl);
            if obj.num_obj > 1
                for k = 0:(obj.num_obj-1)
                    varargout{4*k + 1} = sprintf('x-state %d\n[vel;pos]', k+1);                
                    varargout{4*k + 3} = sprintf('y-state %d\n[vel;pos]', k+1);
                    if obj.vel_ref
                        varargout{4*k + 2} = sprintf('x-reference %d\n[vel;pos]', k+1);
                        varargout{4*k + 4} = sprintf('y-reference %d\n[vel;pos]', k+1);
                    else
                        varargout{4*k + 2} = sprintf('x-reference %d\n(position)', k+1);
                        varargout{4*k + 4} = sprintf('y-reference %d\n(position)', k+1);
                    end
                end
            else
                varargout{1} = sprintf('x-state\n[vel;pos]');                
                varargout{3} = sprintf('y-state\n[vel;pos]');
                if obj.vel_ref
                    varargout{2} = sprintf('x-reference\n[vel;pos]');
                    varargout{4} = sprintf('y-reference\n[vel;pos]');
                else
                    varargout{2} = sprintf('x-reference\n(position)');
                    varargout{4} = sprintf('y-reference\n(position)');
                end
            end
            
            if obj.show_en
                varargout{end} = 'Enable';
            end
        end

        function varargout = getOutputNamesImpl(obj)
            % Return output port names for System block
            varargout = cell(1, obj.num_obj*2);
            if obj.num_obj > 1
                for k = 0:obj.num_obj-1
                    varargout{2*k + 1} = sprintf('F_x %u',k+1);
                    varargout{2*k + 2} = sprintf('F_y %u',k+1);
                end
            else
                varargout{1} = 'F_x';
                varargout{2} = 'F_y';
            end
        end 
        
        function icon = getIconImpl(~)
            % Define a string as the icon for the System block in Simulink.
                icon = sprintf('Multi-object\nLQR');
            
        end
    end   
end
