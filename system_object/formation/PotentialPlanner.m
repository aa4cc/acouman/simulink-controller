classdef PotentialPlanner < matlab.System ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % Potential field planner. 
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties (Nontunable)
        % WIDTH Triangle width
        width single = 14;
        % HEIGHT Triangle height
        height single = 18;
        % GOAL_X Goal x-positions
        goal_x single = [0,0];
        % GOAL_Y Goal y-positions
        goal_y single = [0,0];
        % GOAL_R Goal rotations
        goal_r single = [0,0];
        % EDGES_TOUCH Touching edges
        edges_touch = false(2,3);
        % V_MAX Maximum velocity
        v_max single = 5;
        % OMEGA_MAX Maximum angular velocity
        omega_max single = pi;
        % C_V Retained velocity
        c_v single = 0.25;
        % C_M Virtual inverse inertia
        c_m single = 0.5;
        % C_R Virtual repulsive force coefficient
        c_r single = 40;
        % D_OFF Mutual distance offset
        d_off single = 10;
        % HOME_OFF Homing offset
        home_off single = 15;
    end
    
    properties (Nontunable, PositiveInteger)
        % NUM_OBJ Number of objects
        num_obj = 2;
    end
    
    properties (Access = private)
        home_x
        home_y
        go_home
        in_position
        go_after
        v_x
        v_y
    end
    
    methods
        % Constructor
        function obj = PotentialPlanner(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj)
            obj.go_home = true(obj.num_obj,1);
            obj.in_position = false(obj.num_obj,1);
            obj.home_x = zeros(obj.num_obj,1,'single');
            obj.home_y = zeros(obj.num_obj,1,'single');
            priority = zeros(obj.num_obj,1,'int32');
            
            AB_p = single(obj.home_off * [1;0]);
            BC_dist = sqrt(obj.width^2 + 4*obj.height^2);
            BC_p = obj.home_off * [-obj.width; 2*obj.height] / BC_dist;
            CA_p = obj.home_off * [-obj.width; -2*obj.height] / BC_dist;
            
            for k = int32(1:obj.num_obj)
                rot = obj.goal_r(k);
                rot_mat = [cos(rot), -sin(rot); sin(rot), cos(rot)];
                ab = single(obj.edges_touch(k, 1));
                bc = single(obj.edges_touch(k, 2));
                ca = single(obj.edges_touch(k, 3));
                home_offset = rot_mat * (AB_p*ab + BC_p*bc + CA_p*ca) / (ab+bc+ca);
                obj.home_x(k) = obj.goal_x(k) + home_offset(1);
                obj.home_y(k) = obj.goal_y(k) + home_offset(2);
                
                fprintf('Triangle %d\n', k);
                fprintf('Home (x,y): %g %g \n', obj.home_x(k), obj.home_y(k));
                fprintf('=================== \n');
                
                if all(obj.edges_touch(k, :))
                    priority = priority + 1;
                    priority(k) = 1;
                    obj.go_home(k) = false;
                else
                    priority(k) = k;
                end
            end
            
            obj.go_after = zeros(obj.num_obj,1,'int32');
            for k = int32(1:obj.num_obj)
                if priority(k) == 1
                    obj.go_after(k) = 0;
                else
                    obj.go_after(k) = int32(find(priority==priority(k)-1));
                end
            end
            
            obj.v_x = zeros(obj.num_obj,1,'single');
            obj.v_y = zeros(obj.num_obj,1,'single');
        end
        
        function varargout = stepImpl(obj, varargin)
            X = zeros(obj.num_obj,1, 'single');
            Y = zeros(obj.num_obj,1, 'single');
            R = zeros(obj.num_obj,1, 'single');
            for k = 1:obj.num_obj
                X(k) = varargin{3*k-2};
                Y(k) = varargin{3*k-1};
                R(k) = varargin{3*k};
            end
            
            x_rel = zeros(obj.num_obj, 'single');
            y_rel = zeros(obj.num_obj, 'single');
            D = eye(obj.num_obj, 'single');
            for i = 1:obj.num_obj
                for j = (i+1):obj.num_obj
                    x_rel(i,j) = X(i) - X(j);
                    x_rel(j,i) = - x_rel(i,j);
                    y_rel(i,j) = Y(i) - Y(j);
                    y_rel(j,i) = - y_rel(i,j);
                    D(i,j) = sqrt(x_rel(i,j)^2 + y_rel(i,j)^2) - obj.d_off;
                    D(j,i) = D(i,j);
                end
            end
            
            for k = 1:obj.num_obj
                rel_goal_r = pmpi(obj.goal_r(k) - R(k));
                omega = max(min(rel_goal_r, obj.omega_max/10), -obj.omega_max/10);
                if obj.go_home(k)
                    rel_goal_x = obj.home_x(k) - X(k);
                    rel_goal_y = obj.home_y(k) - Y(k);
                    
                    [obj.v_x(k), obj.v_y(k)] = sat_2D(obj.c_v*obj.v_x(k) + obj.c_m*rel_goal_x + obj.c_r*sum(x_rel(k,:)./D(k,:).^3), ...
                        obj.c_v*obj.v_y(k) + obj.c_m*rel_goal_y + obj.c_r*sum(y_rel(k,:)./D(k,:).^3), obj.v_max);
                    
                    home_dist = rel_goal_x^2 + rel_goal_y^2;
                    if obj.go_after(k) ~= 0
                        can_go = obj.in_position(obj.go_after(k));
                    else
                        can_go = true;
                    end
                    if (home_dist < 4) && (abs(rel_goal_r) < 0.09) && can_go
                        obj.go_home(k) = false;
                    end
                else
                    rel_goal_x = obj.goal_x(k) - X(k);
                    rel_goal_y = obj.goal_y(k) - Y(k);
                    
                    [obj.v_x(k), obj.v_y(k)] = sat_2D(obj.c_v*obj.v_x(k) + obj.c_m*rel_goal_x, ...
                        obj.c_v*obj.v_y(k) + obj.c_m*rel_goal_y, obj.v_max);
                    
                    home_dist = rel_goal_x^2 + rel_goal_y^2;
                    if (home_dist < 4) && (abs(rel_goal_r) < 0.09)
                        obj.in_position(k) = true;
                    end
                end 
                
                varargout{6*k-5} = X(k) + obj.v_x(k);
                varargout{6*k-4} = Y(k) + obj.v_y(k);
                varargout{6*k-3} = R(k) + omega;
                varargout{6*k-2} = obj.v_x(k);
                varargout{6*k-1} = obj.v_y(k);
                varargout{6*k} = omega;
            end
        end
        
        function releaseImpl(obj)

        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(obj)
            num = 3*obj.num_obj;
        end
        
        function num = getNumOutputsImpl(obj)
            num = 6*obj.num_obj;
        end
        
        function varargout = getOutputSizeImpl(obj)
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = 1;
            end
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = 'single';
            end
        end
        
        function varargout = isOutputSizeLockedImpl(obj,~)
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = true;
            end
        end
        
        function varargout = isOutputFixedSizeImpl(obj,~)
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = true;
            end
        end
        
        function varargout = isOutputComplexImpl(obj,~)
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = false;
            end
        end
        
        function varargout = isOutputComplexityLockedImpl(obj,~)
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = true;
            end
        end
        
        
%         function validatePropertiesImpl(obj)
%         end
        
        function icon = getIconImpl(obj)
            % Define a string as the icon for the System block in Simulink.
            icon = sprintf('Potential field\nplanner');            
        end

        function varargout = getOutputNamesImpl(obj)
            for k = 0:obj.num_obj-1
                varargout{6*k+1} = sprintf('Ref x %d', k+1);
                varargout{6*k+2} = sprintf('Ref y %d', k+1);
                varargout{6*k+3} = sprintf('Ref rot %d', k+1);
                varargout{6*k+4} = sprintf('Ref v_x %d', k+1);
                varargout{6*k+5} = sprintf('Ref v_y %d', k+1);
                varargout{6*k+6} = sprintf('Ref omega %d', k+1);
            end
        end
        
        function varargout = getInputNamesImpl(obj)
            for k = 0:obj.num_obj-1
                varargout{3*k+1} = sprintf('x %d', k+1);
                varargout{3*k+2} = sprintf('y %d', k+1);
                varargout{3*k+3} = sprintf('rot %d', k+1);
            end
        end
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', PotentialPlanner.getDescriptiveName());
        end

        function group = getPropertyGroupsImpl
            % Define property section(s) for System block dialog
            n_obj_section = matlab.system.display.Section('Title', 'Objects', 'PropertyList', {'num_obj','width','height'});
            obj_section = matlab.system.display.Section('Title', 'Goal positions', 'PropertyList', {'goal_x','goal_y','goal_r','edges_touch'}, ...
                'Description', ['Enter the goal positions as vectors. You also need to specify, whether any edges of the objects touch ', ...
                'another object. Specify this as a n-by-3 logical matrix, where first column is for edge AB, second column is for BC, ',...
                'and third column is for CA.']);
            pot_section = matlab.system.display.Section('Title', 'Potential field', 'PropertyList', {'c_v','c_m','home_off'});
            rep_section = matlab.system.display.Section('Title', 'Virtual repulsive force', 'PropertyList', {'c_r','d_off'});
            group = [matlab.system.display.SectionGroup('Title', 'Objects settings', 'Sections', [n_obj_section, obj_section]),...
                matlab.system.display.SectionGroup('Title', 'Planner settings', 'Sections', [pot_section, rep_section])];
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Code generation';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Potential field planner';
        end
    end
end
