classdef TrajectoryBreathe < matlab.System & matlab.system.mixin.Propagates
    % TrajectoryBreathe Class that generates trajectory resembling "breathing" - objects go on a circle with an oscillating radius
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    properties (Nontunable)
        % R_MIN Minimal radius
        r_min single = 25;
        % R_MAX Maximal radius
        r_max single = 45;
        % T_ROT Period of rotation
        T_rot single = 30;
        % T_BREATH Period of "breathing"
        T_breath single = 10;
        % TS Sample time
        Ts single = 0.04;
    end
    
    properties (Nontunable, PositiveInteger)
        % NUM_OBJ Number of objects
        num_obj = 1;
    end
    
    properties (Nontunable, Logical)
        % SHOW_EN Show enable port
        show_en = true;
    end

    % Pre-computed constants
    properties(Access = private)
        omega_rot
        omega_breath
        r_center
        r_diff
        phase_shifts
        time single = 0;
    end   
    

    methods(Access = protected)
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants
            obj.omega_rot = 2*pi/obj.T_rot;
            obj.omega_breath = 2*pi/obj.T_breath;
            obj.phase_shifts = (0:obj.num_obj-1) * 2*pi/obj.num_obj;
            obj.r_center = (obj.r_max + obj.r_min) / 2;
            obj.r_diff = (obj.r_max - obj.r_min) / 2;
        end

        function varargout = stepImpl(obj, enable)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            %varargout = cell(1, 4*obj.num_obj);
            if ~obj.show_en
                enable = true;
            end
            
            if enable
                breath_phase = obj.omega_breath * obj.time;
                rot_phase = obj.omega_rot * obj.time;
                R = obj.r_center + obj.r_diff * sin(breath_phase);
                for k = 0:obj.num_obj-1
                    rot_phase_k = rot_phase + obj.phase_shifts(k+1);
                    varargout{4*k + 1} = R * cos(rot_phase_k);
                    varargout{4*k + 2} = R * sin(rot_phase_k);
                    varargout{4*k + 3} = - obj.omega_rot * R * sin(rot_phase_k) + ...
                        obj.omega_breath * obj.r_diff * cos(breath_phase) * cos(rot_phase_k);
                    varargout{4*k + 4} =   obj.omega_rot * R * cos(rot_phase_k) + ...
                        obj.omega_breath * obj.r_diff * cos(breath_phase) * cos(rot_phase_k);
                end
                obj.time = obj.time + obj.Ts;
            else
                for k = 1:4*obj.num_obj
                    varargout{k} = single(0);
                end
                obj.time = single(0);
            end
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
            obj.time = single(0);
        end

        function validateInputsImpl(obj,enable)
            % Validate inputs to the step method at initialization
            if obj.show_en
                validateattributes(enable, {'logical'}, {'scalar'})
            end
        end

        function num = getNumInputsImpl(obj)
            % Define total number of inputs for system with optional inputs
            if obj.show_en
                num = 1;
            else
                num = 0;
            end
        end

        function num = getNumOutputsImpl(obj)
            % Define total number of outputs for system with optional
            % outputs
            num = obj.num_obj * 4;
        end 
        
        function varargout = getOutputSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,4*obj.num_obj);
            for k = 1:4*obj.num_obj
                varargout{k} = [1 1];
            end
        end
        
        function varargout = isOutputFixedSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,4*obj.num_obj);
            for k = 1:4*obj.num_obj
                varargout{k} = true;
            end
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            % Return size for each output port
            varargout = cell(1,4*obj.num_obj);
            for k = 1:4*obj.num_obj
                varargout{k} = 'single';
            end
        end
        
        function varargout = isOutputComplexImpl(obj)
            % Return size for each output port
            varargout = cell(1,4*obj.num_obj);
            for k = 1:4*obj.num_obj
                varargout{k} = false;
            end
        end
        
        function varargout = isOutputComplexityLockedImpl(obj)
            % Return size for each output port
            varargout = cell(1,4*obj.num_obj);
            for k = 1:4*obj.num_obj
                varargout{k} = true;
            end
        end

        function name = getInputNamesImpl(~)
            % Return input port names for System block
            name = 'Enable';
        end

        function varargout = getOutputNamesImpl(obj)
            % Return output port names for System block
            varargout = cell(1, obj.num_obj*4);
            if obj.num_obj > 1
                for k = 0:obj.num_obj-1
                    varargout{4*k + 1} = sprintf('x %u',k+1);
                    varargout{4*k + 2} = sprintf('y %u',k+1);
                    varargout{4*k + 3} = sprintf('v_x %u',k+1);
                    varargout{4*k + 4} = sprintf('v_y %u',k+1);
                end
            else
                varargout{1} = 'x';
                varargout{2} = 'y';
                varargout{3} = 'v_x';
                varargout{4} = 'v_y';
            end
        end 
        
        function icon = getIconImpl(~)
            % Define a string as the icon for the System block in Simulink.
                icon = sprintf('Breathing\ntrajectory generator');
            
        end
    end   
end
