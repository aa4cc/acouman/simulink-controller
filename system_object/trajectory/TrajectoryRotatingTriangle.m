classdef TrajectoryRotatingTriangle < matlab.System & matlab.system.mixin.Propagates
    % TrajectoryRotatingTriangle Class that generates a trajectory for triangle.
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    properties (Nontunable)
        % C_X Orbit x-center
        c_x single = 0;
        % C_Y Orbit y-center
        c_y single = 0;
        % R_O Radius of orbit
        r_o single = 25;
        % T_O Period of orbit
        T_o single = 30;
        % T_R Period of rotation
        T_r single = 10;
        % TS Sample time
        Ts single = 0.04;
    end
    
    properties (Nontunable, PositiveInteger)
        % NUM_OBJ Number of objects
        num_obj = 1;
    end
    
    properties (Nontunable, Logical)
        % SHOW_EN Show enable port
        show_en = true;
        % ORB_PHASE Objects have different orbit phase
        orb_phase = true;
        % ROT_PHASE Objects have different rotation phase
        rot_phase = true;
    end

    % Pre-computed constants
    properties(Access = private)
        C_x single
        C_y single
        R_o single
        omega_rot single
        omega_orb single
        rot_shifts single
        orb_shifts single
        time single = single(0);
    end
    
    

    methods(Access = protected)
        function setupImpl(obj)
            % Perform one-time calculations, such as computing constants
            obj.omega_rot = 2*pi/obj.T_r;
            obj.omega_orb = 2*pi/obj.T_o;
            if obj.rot_phase
                obj.rot_shifts = single((0:obj.num_obj-1) * 2*pi/obj.num_obj);
            else
                obj.rot_shifts = zeros(1, obj.num_obj, 'single');
            end
            if obj.orb_phase
                obj.orb_shifts = single((0:obj.num_obj-1) * 2*pi/obj.num_obj);
            else
                obj.orb_shifts = zeros(1, obj.num_obj, 'single');
            end
            if obj.num_obj > 1
                if numel(obj.c_x) == 1
                    obj.C_x = obj.c_x * ones(1, obj.num_obj, 'single');
                else
                    obj.C_x = obj.c_x;
                end
                if numel(obj.c_y) == 1
                    obj.C_y = obj.c_y * ones(1, obj.num_obj, 'single');
                else
                    obj.C_y = obj.c_y;
                end
                if numel(obj.r_o) == 1
                    obj.R_o = obj.r_o * ones(1, obj.num_obj, 'single');
                else
                    obj.R_o = obj.r_o;
                end
            else
                obj.C_x = obj.c_x;
                obj.C_y = obj.c_y;
                obj.R_o = obj.r_o;
            end
        end

        function varargout = stepImpl(obj, enable)
            % Implement algorithm. Calculate y as a function of input u and
            % discrete states.
            %varargout = cell(1, 4*obj.num_obj);
            if ~obj.show_en
                enable = true;
            end
            if enable
                for k = 0:obj.num_obj-1
                    varargout{6*k + 1} = obj.C_x(k+1) + obj.R_o(k+1) * ...
                        cos(obj.omega_orb*obj.time + obj.orb_shifts(k+1));
                    varargout{6*k + 2} = obj.C_y(k+1) + obj.R_o(k+1) * ...
                        sin(obj.omega_orb*obj.time + obj.orb_shifts(k+1));
                    varargout{6*k + 3} = obj.rot_shifts(k+1) + ...
                        obj.omega_rot * obj.time;
                    varargout{6*k + 4} = - obj.R_o(k+1) * obj.omega_orb * ...
                        sin(obj.omega_orb*obj.time + obj.orb_shifts(k+1));
                    varargout{6*k + 5} = obj.R_o(k+1) * obj.omega_orb * ...
                        cos(obj.omega_orb*obj.time + obj.orb_shifts(k+1));
                    varargout{6*k + 6} = obj.omega_rot;
                end
                obj.time = obj.time + obj.Ts;
            else
                for k = 1:6*obj.num_obj
                    varargout{k} = single(0);
                end
                obj.time = single(0);
            end
        end

        function resetImpl(obj)
            % Initialize / reset discrete-state properties
            obj.time = single(0);
        end

        function validateInputsImpl(obj,enable)
            % Validate inputs to the step method at initialization
            if obj.show_en
                validateattributes(enable, {'logical'}, {'scalar'})
            end
        end

        function num = getNumInputsImpl(obj)
            % Define total number of inputs for system with optional inputs
            if obj.show_en
                num = 1;
            else
                num = 0;
            end
        end

        function num = getNumOutputsImpl(obj)
            % Define total number of outputs for system with optional
            % outputs
            num = obj.num_obj * 6;
        end 
        
        function varargout = getOutputSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,6*obj.num_obj);
            for k = 1:6*obj.num_obj
                varargout{k} = [1 1];
            end
        end
        
        function varargout = isOutputFixedSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,6*obj.num_obj);
            for k = 1:6*obj.num_obj
                varargout{k} = true;
            end
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            % Return size for each output port
            varargout = cell(1,6*obj.num_obj);
            for k = 1:6*obj.num_obj
                varargout{k} = 'single';
            end
        end
        
        function varargout = isOutputComplexImpl(obj)
            % Return size for each output port
            varargout = cell(1,6*obj.num_obj);
            for k = 1:6*obj.num_obj
                varargout{k} = false;
            end
        end
        
        function varargout = isOutputComplexityLockedImpl(obj)
            % Return size for each output port
            varargout = cell(1,6*obj.num_obj);
            for k = 1:6*obj.num_obj
                varargout{k} = true;
            end
        end

        function name = getInputNamesImpl(~)
            % Return input port names for System block
            name = 'Enable';
        end

        function varargout = getOutputNamesImpl(obj)
            % Return output port names for System block
            varargout = cell(1, obj.num_obj*4);
            if obj.num_obj > 1
                for k = 0:obj.num_obj-1
                    varargout{6*k + 1} = sprintf('x %u',k+1);
                    varargout{6*k + 2} = sprintf('y %u',k+1);
                    varargout{6*k + 3} = sprintf('theta %u',k+1);
                    varargout{6*k + 4} = sprintf('v_x %u',k+1);
                    varargout{6*k + 5} = sprintf('v_y %u',k+1);
                    varargout{6*k + 6} = sprintf('omega %u',k+1);
                end
            else
                varargout{1} = 'x';
                varargout{2} = 'y';
                varargout{3} = 'theta';
                varargout{4} = 'v_x';
                varargout{5} = 'v_y';
                varargout{6} = 'omega';
            end
        end 
        
        function icon = getIconImpl(~)
            % Define a string as the icon for the System block in Simulink.
                icon = sprintf('Orbiting triangles\ntrajectory generator');
            
        end
    end   
end
