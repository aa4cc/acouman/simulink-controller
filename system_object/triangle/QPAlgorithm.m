classdef QPAlgorithm < Simulink.IntEnumType
    enumeration
        quadprog                        (0)
        projected_gradient              (1)
        accelerated_projected_gradient  (2)
    end
    methods (Static)
        function retVal = getDefaultValue()
            retVal = QPAlgorithm.quadprog;
        end
  end
end
