function [U,P, F_actual, T_actual] = descentmpc(A_t_bar, C_t_col, A_r_bar, C_r_col, QC_t_col, QC_r_col, R, rot, x0, h_ctrl, forces, torques, p_min, p_max, qp_alg)
    N = int32(size(forces, 2));
    rot_mat = [cos(rot), -sin(rot); sin(rot), cos(rot)];
    
    u = randi([1,N], h_ctrl, 1, 'int32');
    U = u(1);
    v_u = single(20)*randn(h_ctrl, 1, 'single');
    F = rot_mat * forces(:,u);
    T = torques(:,u);
    [p, J] = tri_mpc(A_t_bar, C_t_col, A_r_bar, C_r_col, QC_t_col, QC_r_col, R, [F; T], x0, p_min, p_max, qp_alg);
    P = single(p(1));
    F_actual = F(:, 1)*P;
    T_actual = T(1)*P;
    
    n_iter = int32(5)*h_ctrl;
    
    K_v = single(5);
    
    w = single(0.8);
    for k = int32(1):n_iter       
        v_u = w*v_u + K_v*single(U - u);
        for i = int32(1):h_ctrl
            u(i) = mod(u(i) + int32(v_u(i)) - 1, N) + 1;
        end
        F = rot_mat * forces(:,u);
        T = torques(:,u);
        [p,j] = tri_mpc(A_t_bar, C_t_col, A_r_bar, C_r_col, QC_t_col, QC_r_col, R, [F; T], x0, p_min, p_max, qp_alg);
        if j <= J
            U = u(1);
            P = single(p(1));
            J = j;
            F_actual = F(:, 1)*P;
            T_actual = T(1)*P;
        end
    end
end