function [x_opt, opt_val, iter_count] = grad_proj_solver(H, F, ub, lb, x0)
% min 1/2 z'*H*z + F'*z
%  s.t. lb <= z <= ub
%
% http://cse.lab.imtlucca.it/~bemporad/teaching/mpc/imt/3-qp_explicit.pdf
% slide 10

L = norm(H,'fro');
N_iter = 20;

for k=1:N_iter
    step = 1/L*(H*x0 + F);
    x_new = max(lb, min(ub, x0 - step));
    
    if all(abs(x_new - x0) < 1e-3)
%         disp('finished early')
        break;
    end
end

x_opt = x_new;

if nargout > 1
    opt_val = 1/2*x_opt'*H*x_opt + F'*x_opt;
end

if nargout > 2
    iter_count = k;
end

end

