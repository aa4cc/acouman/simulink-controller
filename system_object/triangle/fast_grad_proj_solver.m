function [x_opt, opt_val, iter_count] = fast_grad_proj_solver(H, F, ub, lb, x0)
% min 1/2 z'*H*z + F'*z
%  s.t. lb <= z <= ub
%
% http://cse.lab.imtlucca.it/~bemporad/teaching/mpc/imt/3-qp_explicit.pdf
% slide 12

L = norm(H,'fro');
N_iter = single(20);
zk_m1 = x0;
zk = x0;

for k=single(1):N_iter
    beta_k = max((k-1)/(k+2),0);
    sk_p1 = zk + beta_k*(zk-zk_m1);
    
    step = 1/L*(H*sk_p1 + F);
    zk_p1 = max(lb, min(ub, sk_p1 - step));
    
    zk_m1 = zk;
    zk = zk_p1;
    
    if all(abs(zk_m1 - zk) < 1e-3)
%         disp('finished early')
        break;
    end
end

x_opt = zk;

if nargout > 1
    opt_val = 1/2*x_opt'*H*x_opt + F'*x_opt;
end

if nargout > 2
    iter_count = k;
end

end

