function [P, J_opt] = tri_mpc(A_t, B_t, A_r, B_r, Q_t, Q_r, R, U, x0, h_pred, p_min, p_max)
%     load forces_full_volume
%     U = [forces(:,[1,10]); torques(:,[1,10])];
    h_ctrl = int32(size(U, 2)); % control horizon
%     h_pred = int32(5); % prediction horizon
    pmin = p_min * ones(h_ctrl, 1, 'single');
    pmax = p_max * ones(h_ctrl, 1, 'single');
    
    O = zeros(2, 'single');
    Q = [Q_t O O; O Q_t O; O O Q_r];
%     R = 1e-8;
    
    A_big = [A_t O O; O A_t O; O O A_r];    
    
    A_bar = zeros(6*h_pred, 6, 'single');
    for i = int32(1):h_pred
        A_bar((6*i-5):(6*i), :) = A_big^single(i);
    end
    
    C_bar = zeros(6*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        B_big = [B_t*U(1,i); B_t*U(2,i); B_r*U(3,i)];
        for j = int32(0):(h_pred-i)
            C_bar((6*(j+i)-5):(6*(j+i)), i) = (A_big^single(j)) * B_big;
        end
    end
    
    Q_bar = kron(eye(h_pred, 'single'), Q);
    R_bar = R*eye(h_ctrl, 'single');
    
    H = C_bar'*Q_bar*C_bar + R_bar;
    F = A_bar'*Q_bar*C_bar;
    
%     disp('F = ')
%     disp(F)
%     disp('H = ')
%     disp(H)
     
    opts = optimoptions('quadprog');
    opts.Display = 'none';
    opts.Algorithm = 'active-set';
    [P,J_opt] = quadprog(double(H), double(F'*x0), [], [], [], [], double(pmin), double(pmax), double(pmax-pmin)/2, opts);
