%% simulation parameters
load forces_full_volume.mat
run ..\..\controller_constants\Triangle\full_volume_model.m
x_x0 = single([0;10]);
x_y0 = single([-1;5]);
x_r0 = single([0;1]);

x_xref = single([0;0]);
x_yref = single([0;0]);
x_rref = single([0;0]);

tf = 10;
Ts = 0.04;
t = 0:Ts:tf;

Q_t = single(diag([1,10]) * 1e1);
Q_r = single(diag([1,10]) * 1e3);
R = single(1e-8);

h_ctrl = int32(5);
h_pred = int32(10);

qp_alg = QPAlgorithm.accelerated_projected_gradient;

%% pre-computed matrices
A_t = single(A_t);
A_r = single(A_r);
B_t = single(B_t);
B_r = single(B_r);
forces = single(forces);
torques = single(torques);

A_t_bar = zeros(2*h_pred, 2, 'single');
A_t_bar(1:2, :) = A_t;
for i = int32(1):(h_pred-1)
    A_t_bar((2*i+1):(2*i+2), :) = A_t * A_t_bar((2*i-1):(2*i), :);
end

A_r_bar = zeros(2*h_pred, 2, 'single');
A_r_bar(1:2, :) = A_r;
for i = int32(1):(h_pred-1)
    A_r_bar((2*i+1):(2*i+2), :) = A_r * A_r_bar((2*i-1):(2*i), :);
end

C_t_col = zeros(2*h_pred, 1, 'single');
C_t_col(1:2) = B_t;
for i = int32(1):(h_pred-1)
    C_t_col((2*i+1):(2*i+2)) = A_t_bar((2*i-1):(2*i), :) * B_t;
end

C_r_col = zeros(2*h_pred, 1, 'single');
C_r_col(1:2) = B_r;
for i = int32(1):(h_pred-1)
    C_r_col((2*i+1):(2*i+2)) = A_r_bar((2*i-1):(2*i), :) * B_r;
end

QC_t_col = kron(eye(h_pred, 'single'), Q_t) * C_t_col;
QC_r_col = kron(eye(h_pred, 'single'), Q_r) * C_r_col;

%% run simulation
N = numel(t);
x_x = zeros(2,N,'single');
x_x(:,1) = x_x0;
x_y = zeros(2,N,'single');
x_y(:,1) = x_y0;
x_r = zeros(2,N,'single');
x_r(:,1) = x_r0;
P = zeros(1,N,'single');

% profile on
for k = 1:N-1
    rot = x_r(2,k);
    state_diff = [x_x(:,k)-x_xref; x_y(:,k)-x_yref; x_r(1,k)-x_rref(1); pmpi(rot-x_rref(2))];
    [~,p,F,T] = descentmpc(A_t_bar, C_t_col, A_r_bar, C_r_col, QC_t_col, QC_r_col, R, rot, state_diff, h_ctrl, forces, torques, single(0), single(1.8), qp_alg);
%     [~,p,F,T] = descentmpc_mex(A_t_bar, C_t_col, A_r_bar, C_r_col, QC_t_col, QC_r_col, R, rot, state_diff, h_ctrl, forces, torques, single(0), single(1.8), qp_alg);
    P(k) = p(1);
    fx = F(1,1);
    fy = F(2,1);
    t = T(1);
    x_x(:,k+1) = A_t*x_x(:,k) + B_t*fx;
    x_y(:,k+1) = A_t*x_y(:,k) + B_t*fy;
    x_r(:,k+1) = A_r*x_r(:,k) + B_r*t;
end
% profile viewer
%% show animation
x0 = [-18/3, -18/3, 2*18/3];
y0 = [14/2, -14/2, 0];
figure
tri_plot = patch(x0, y0, 'red');
xlim([-70,70])
ylim([-70,70])
for k = 1:N
    tic
    r = x_r(2, k);
    rot_mat = [cos(r), -sin(r); sin(r), cos(r)];
    points = [x_x(2,k); x_y(2,k)] + rot_mat*[x0;y0];
    tri_plot.Vertices = points';
    
    pause(Ts - toc)
end