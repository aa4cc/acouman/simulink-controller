classdef OptMethod < Simulink.IntEnumType
    enumeration
        complete_search   (0)
        PSO               (1)
    end
    methods (Static)
        function retVal = getDefaultValue()
            retVal = OptMethod.complete_search;
        end
  end
end
