classdef TriangleMPC < matlab.System ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % Model predictive controller for triangles
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties (Nontunable)
        % MODEL Model of acoustophoretic force 
        model (1,1) ForceModel
        % QP_ALG Quadratic program algorithm
        qp_alg (1,1) QPAlgorithm
        % A_T A matrix
        A_t single = [-1 0; 1 0];
        % B_T B matrix
        B_t single = [1; 0];
        % Q_T Q matrix
        Q_t single = [1 0;0 10];
        % A_R A matrix
        A_r single = [-1 0; 1 0];
        % B_R B matrix
        B_r single = [1; 0];
        % Q_R Q matrix
        Q_r single = [1 0;0 10];
        % R R matrix
        R single = 1;
        % SAT Control saturation
        sat single = 1.8;
    end
    
    properties (Nontunable, PositiveInteger)
        % H_CTRL Control horizon
        h_ctrl = 5;
        % H_PRED Prediction horizon
        h_pred = 10;
    end
    
    properties (Nontunable, Logical)
        % SHOW_ACTUAL Output achieved forces and torques
        show_actual = false
    end
    
    properties (Access = private)
        forces
        torques
        points
        A_t_bar
        A_r_bar
        C_t_col
        C_r_col
        QC_t_col
        QC_r_col
    end
    
    methods
        % Constructor
        function obj = TriangleMPC(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj)
            switch obj.model
                case ForceModel.full_volume
                    filename = "forces_full_volume";
                case ForceModel.along_edge
                    filename = "forces_along_edge";
                otherwise
                    filename = "forces_closest_point";
            end
            S = coder.load(filename);
            obj.points = single(S.points);
            obj.forces = single(S.forces);
            obj.torques = single(S.torques);
            
            obj.A_t_bar = zeros(2*obj.h_pred, 2, 'single');
            obj.A_t_bar(1:2, :) = obj.A_t;
            for i = 1:(obj.h_pred-1)
                obj.A_t_bar((2*i+1):(2*i+2), :) = obj.A_t * obj.A_t_bar((2*i-1):(2*i), :);
            end

            obj.A_r_bar = zeros(2*obj.h_pred, 2, 'single');
            obj.A_r_bar(1:2, :) = obj.A_r;
            for i = 1:(obj.h_pred-1)
                obj.A_r_bar((2*i+1):(2*i+2), :) = obj.A_r * obj.A_r_bar((2*i-1):(2*i), :);
            end

            obj.C_t_col = zeros(2*obj.h_pred, 1, 'single');
            obj.C_t_col(1:2) = obj.B_t;
            for i = 1:(obj.h_pred-1)
                obj.C_t_col((2*i+1):(2*i+2)) = obj.A_t_bar((2*i-1):(2*i), :) * obj.B_t;
            end

            obj.C_r_col = zeros(2*obj.h_pred, 1, 'single');
            obj.C_r_col(1:2) = obj.B_r;
            for i = 1:(obj.h_pred-1)
                obj.C_r_col((2*i+1):(2*i+2)) = obj.A_r_bar((2*i-1):(2*i), :) * obj.B_r;
            end
            
            obj.QC_t_col = kron(eye(obj.h_pred, 'single'), obj.Q_t) * obj.C_t_col;
            obj.QC_r_col = kron(eye(obj.h_pred, 'single'), obj.Q_r) * obj.C_r_col;
        end
        
        function [P_x, P_y, P, Fx_actual, Fy_actual, T_actual] = stepImpl(obj, x_state, y_state, rot_state, x_ref, y_ref, rot_ref)  
            rot = rot_state(2);
            c = cos(rot);
            s = sin(rot);
            
            state_diff = [x_state-x_ref; y_state-y_ref; rot_state(1)-rot_ref(1); pmpi(rot-rot_ref(2))];
            
            [k, P, F_actual, T_actual] = descentmpc(obj.A_t_bar, obj.C_t_col, obj.A_r_bar, obj.C_r_col, obj.QC_t_col, obj.QC_r_col, ...
                obj.R, rot, state_diff, int32(obj.h_ctrl), obj.forces, obj.torques, single(0), obj.sat, obj.qp_alg);
            
            P_x_t = obj.points(1, k);
            P_y_t = obj.points(2, k);
            
            P_x = (x_state(2) + P_x_t*c - P_y_t*s) * 1e-3;
            P_y = (y_state(2) + P_y_t*c + P_x_t*s) * 1e-3;
            
            Fx_actual = F_actual(1);
            Fy_actual = F_actual(2);
            
            P = P * 1e3 + 700;            
        end
        
        function releaseImpl(obj)

        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 6;
        end
        
        function num = getNumOutputsImpl(obj)
            if obj.show_actual
                num = 6;
            else
                num = 3;
            end
        end
        
        function varargout = getOutputSizeImpl(obj)
            varargout{1} = 1;
            varargout{2} = 1;
            varargout{3} = 1;
            if obj.show_actual
                varargout{4} = 1;
                varargout{5} = 1;
                varargout{6} = 1;
            end
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            varargout{1} = 'single';
            varargout{2} = 'single';
            varargout{3} = 'single';
            if obj.show_actual
                varargout{4} = 'single';
                varargout{5} = 'single';
                varargout{6} = 'single';
            end
        end
        
        function varargout = isOutputSizeLockedImpl(obj,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            if obj.show_actual
                varargout{4} = true;
                varargout{5} = true;
                varargout{6} = true;
            end
        end
        
        function varargout = isOutputFixedSizeImpl(obj,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            if obj.show_actual
                varargout{4} = true;
                varargout{5} = true;
                varargout{6} = true;
            end
        end
        
        function varargout = isOutputComplexImpl(obj,~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
            if obj.show_actual
                varargout{4} = false;
                varargout{5} = false;
                varargout{6} = false;
            end
        end
        
        function varargout = isOutputComplexityLockedImpl(obj,~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
            if obj.show_actual
                varargout{4} = false;
                varargout{5} = false;
                varargout{6} = false;
            end
        end
        
        
%         function validatePropertiesImpl(obj)
%         end
        
        function icon = getIconImpl(obj)
            % Define a string as the icon for the System block in Simulink.
            icon = sprintf('Triangle MPC');            
        end

        function varargout = getOutputNamesImpl(obj)
            varargout{1} = 'Point x';
            varargout{2} = 'Point y';
            varargout{3} = 'Amplitude';
            if obj.show_actual
                varargout{4} = 'F_x actual';
                varargout{5} = 'F_y actual';
                varargout{6} = 'Torque actual';
            end
        end
        
        function varargout = getInputNamesImpl(obj)
            varargout{1} = 'x-state';
            varargout{2} = 'y-state';
            varargout{3} = 'rotation-state';
            varargout{4} = 'x-reference';
            varargout{5} = 'y-reference';
            varargout{6} = 'rotation reference';
        end
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', TriangleMPC.getDescriptiveName());
        end

        function group = getPropertyGroupsImpl
            % Define property section(s) for System block dialog
            group = [matlab.system.display.Section('Title', 'MPC settings', 'PropertyList', {'model','qp_alg','h_ctrl','h_pred'});
                matlab.system.display.Section('Title', 'Translational model', 'PropertyList', {'A_t','B_t','Q_t'});
                matlab.system.display.Section('Title', 'Rotational model', 'PropertyList', {'A_r','B_r','Q_r'});
                matlab.system.display.Section('Title', 'Output settings', 'PropertyList', {'R','sat'});
                matlab.system.display.Section('Title', 'Block settings', 'PropertyList', {'show_actual'})];
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Code generation';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Triangle MPC';
        end
    end
end
