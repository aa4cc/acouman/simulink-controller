function [P, J_opt] = tri_mpc(A_t_bar, C_t_col, A_r_bar, C_r_col, QC_t_col, QC_r_col, R, U, x0, p_min, p_max, qp_alg)

    h_ctrl = int32(size(U, 2)); % control horizon
    h_pred = int32(size(A_t_bar, 1)/2);
    pmin = p_min * ones(h_ctrl, 1, 'single');
    pmax = p_max * ones(h_ctrl, 1, 'single');

    C_x = zeros(2*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        C_x((2*i-1):end,i) = U(1,i) * C_t_col(1:(end-2*i+2));
    end
    C_y = zeros(2*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        C_y((2*i-1):end,i) = U(2,i) * C_t_col(1:(end-2*i+2));
    end
    C_r = zeros(2*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        C_r((2*i-1):end,i) = U(3,i) * C_r_col(1:(end-2*i+2));
    end
    
    QC_x = zeros(2*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        QC_x((2*i-1):end,i) = U(1,i) * QC_t_col(1:(end-2*i+2));
    end
    QC_y = zeros(2*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        QC_y((2*i-1):end,i) = U(2,i) * QC_t_col(1:(end-2*i+2));
    end
    QC_r = zeros(2*h_pred, h_ctrl, 'single');
    for i = int32(1):h_ctrl
        QC_r((2*i-1):end,i) = U(3,i) * QC_r_col(1:(end-2*i+2));
    end

%     disp('C_x = ')
%     disp(C_x)
%     disp('C_y = ')
%     disp(C_y)
%     disp('C_r = ')
%     disp(C_r)
    
    R_bar = R*eye(h_ctrl, 'single');
    
    H = C_x'*QC_x + C_y'*QC_y + C_r'*QC_r + R_bar;
    F = [A_t_bar'*QC_x; A_t_bar'*QC_y; A_r_bar'*QC_r];
    
%     disp('F = ')
%     disp(F)
%     disp('H = ')
%     disp(H)
    
    switch qp_alg
        case QPAlgorithm.quadprog
            opts = optimoptions('quadprog');
            opts.Display = 'none';
            opts.Algorithm = 'active-set';
            [P, J] = quadprog(double(H), double(F'*x0), [], [], [], [], double(pmin), double(pmax), double(pmax-pmin)/2, opts);
            J_opt = single(J);
        case QPAlgorithm.projected_gradient
            [P, J_opt] = grad_proj_solver(H, F'*x0, pmax, pmin, (pmax-pmin)/2);
        case QPAlgorithm.accelerated_projected_gradient
            [P, J_opt] = fast_grad_proj_solver(H, F'*x0, pmax, pmin, (pmax-pmin)/2);
        otherwise
            error('Undefined algorithm option')
    end
    
%     fprintf('quadprog: f=%6.2f, gradproj(%d): f=%6.2f, fastgradproj(%d): f=%6.2f\n', J_opt, n_iter_2, J_opt_2, n_iter_3, J_opt_3);
    