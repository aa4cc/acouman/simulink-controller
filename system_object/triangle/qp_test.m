%% simulation parameters
load forces_full_volume.mat
run ..\..\controller_constants\Triangle\full_volume_model.m
x_diff = single([0;1;-0.1;0.8;0;0.1]);

tf = 10;
Ts = 0.04;
t = 0:Ts:tf;

Q_t = single(diag([1,10]) * 1e1);
Q_r = single(diag([1,10]) * 1e3);
R = single(1);

h_ctrl = int32(5);
h_pred = int32(10);

pmin = zeros(h_ctrl, 1, 'single');
pmax = 1.8 * ones(h_ctrl, 1, 'single');

%% pre-computed matrices
A_t = single(A_t);
A_r = single(A_r);
B_t = single(B_t);
B_r = single(B_r);
forces = single(forces);
torques = single(torques);

A_t_bar = zeros(2*h_pred, 2, 'single');
A_t_bar(1:2, :) = A_t;
for i = int32(1):(h_pred-1)
    A_t_bar((2*i+1):(2*i+2), :) = A_t * A_t_bar((2*i-1):(2*i), :);
end

A_r_bar = zeros(2*h_pred, 2, 'single');
A_r_bar(1:2, :) = A_r;
for i = int32(1):(h_pred-1)
    A_r_bar((2*i+1):(2*i+2), :) = A_r * A_r_bar((2*i-1):(2*i), :);
end

C_t_col = zeros(2*h_pred, 1, 'single');
C_t_col(1:2) = B_t;
for i = int32(1):(h_pred-1)
    C_t_col((2*i+1):(2*i+2)) = A_t_bar((2*i-1):(2*i), :) * B_t;
end

C_r_col = zeros(2*h_pred, 1, 'single');
C_r_col(1:2) = B_r;
for i = int32(1):(h_pred-1)
    C_r_col((2*i+1):(2*i+2)) = A_r_bar((2*i-1):(2*i), :) * B_r;
end

QC_t_col = kron(eye(h_pred, 'single'), Q_t) * C_t_col;
QC_r_col = kron(eye(h_pred, 'single'), Q_r) * C_r_col;

R_bar = R*eye(h_ctrl, 'single');

%% solve QP for random points
% u = randi([1 300], 1, h_ctrl);
Fx = forces(1, u);
Fy = forces(2, u);
T = torques(1, u);

C_x = zeros(2*h_pred, h_ctrl, 'single');
for i = int32(1):h_ctrl
    C_x((2*i-1):end,i) = Fx(i) * C_t_col(1:(end-2*i+2));
end
C_y = zeros(2*h_pred, h_ctrl, 'single');
for i = int32(1):h_ctrl
    C_y((2*i-1):end,i) = Fy(i) * C_t_col(1:(end-2*i+2));
end
C_r = zeros(2*h_pred, h_ctrl, 'single');
for i = int32(1):h_ctrl
    C_r((2*i-1):end,i) = T(i) * C_r_col(1:(end-2*i+2));
end

QC_x = zeros(2*h_pred, h_ctrl, 'single');
for i = int32(1):h_ctrl
    QC_x((2*i-1):end,i) = Fx(i) * QC_t_col(1:(end-2*i+2));
end
QC_y = zeros(2*h_pred, h_ctrl, 'single');
for i = int32(1):h_ctrl
    QC_y((2*i-1):end,i) = Fy(i) * QC_t_col(1:(end-2*i+2));
end
QC_r = zeros(2*h_pred, h_ctrl, 'single');
for i = int32(1):h_ctrl
    QC_r((2*i-1):end,i) = T(i) * QC_r_col(1:(end-2*i+2));
end

H = C_x'*QC_x + C_y'*QC_y + C_r'*QC_r + R_bar;
F = [A_t_bar'*QC_x; A_t_bar'*QC_y; A_r_bar'*QC_r];

    disp('F = ')
    disp(F)
    disp('H = ')
    disp(H)

profile on
for i = int32(1):1000
    [P_quadprog, J_quadprog] = quadprog_mex(H, F'*x_diff, (pmax-pmin)/2, pmin, pmax);
    [P_grad, J_grad] = grad_proj_solver_mex(H, F'*x_diff, pmax, pmin, (pmax-pmin)/2);
    [P_fast, J_fast] = fast_grad_proj_solver_mex(H, F'*x_diff, pmax, pmin, (pmax-pmin)/2);
end
profile viewer

if h_ctrl == 2
    [P1,P2] = meshgrid(linspace(0,1.8,100),linspace(0,1.8,100));
    J = zeros(100,100);
    for i = 1:100
        for j = 1:100
            z = [P1(i,j); P2(i,j)];
            J(i,j) = 0.5*z'*H*z + x_diff'*F*z;
        end
    end

    figure(1)
    clf
    surf(P1,P2,J)
    hold on
    scatter3(P_quadprog(1), P_quadprog(2), J_quadprog, 'bx', 'LineWidth', 2)
    plot3(P_hist(1,:), P_hist(2,:), J_hist, '-xr')
    scatter3(P_grad(1), P_grad(2), J_grad, 'kx', 'LineWidth', 2)
end