classdef ForceModel < Simulink.IntEnumType
    enumeration
        full_volume   (0)
        along_edge    (1)
        closest_point (2)
    end
    methods (Static)
        function retVal = getDefaultValue()
            retVal = ForceModel.full_volume;
        end
  end
end
