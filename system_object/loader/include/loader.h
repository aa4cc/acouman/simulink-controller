#ifndef LOADER_H
#define LOADER_H

#include <stdlib.h>

// Put this in setup impl. Loads variables from binary files
void loadUp_double(const char* corrFile, const char* HUnrotFile, const char* heightFile);
void loadUp_single(const char* corrFile, const char* HUnrotFile, const char* heightFile);

// Put these in step impl. 
// Returns the loaded correction
double getCorrection_d();
float getCorrection_s();
// Returns the rotation matrix
void copyHUnrot_d(double *destination);
void copyHUnrot_s(float *destination);
// Returns the platform height
double getHeight_d();
float getHeight_s();

// // Execute at termination
// void loaderTerminate();
#endif
