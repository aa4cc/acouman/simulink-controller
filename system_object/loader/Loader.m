classdef Loader < matlab.System ...
        & coder.ExternalDependency ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % Loads platform configuration files
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties (Nontunable)
        % Path to correction file
        corrFile = '/home/pi/correction.bin';
        % Path to rotation matrix file
        H_unFile = '/home/pi/H_unrot.bin';
        % Path to platform height file
        heightFile = '/home/pi/height.bin';
        % Data type of input files
        dtype (1,1) DataTypeEnum = DataTypeEnum.single_precision;
    end
    
    properties
        
    end
    
    properties (Nontunable, Logical)
    
    end

    properties(Constant, Hidden)
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    methods
        % Constructor
        function obj = Loader(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj)
            if ~isempty(coder.target)
                coder.cinclude('loader.h');
                if obj.dtype == DataTypeEnum.double_precision
                    coder.ceval('loadUp_double',[obj.corrFile 0],[obj.H_unFile 0],[obj.heightFile 0]);                    
                else
                    coder.ceval('loadUp_single',[obj.corrFile 0],[obj.H_unFile 0],[obj.heightFile 0]);
                end
            end
        end
        
        function [corr,H_unrot,height] = stepImpl(obj)
            if obj.dtype == DataTypeEnum.double_precision
                corr = 0;
                H_unrot = zeros(3,3);
                height = 0;
                if ~isempty(coder.target)               
                    corr = coder.ceval('getCorrection_d');
                    coder.ceval('copyHUnrot_d',coder.wref(H_unrot));
                    height = coder.ceval('getHeight_d');
                end
            else
                corr = single(0);
                H_unrot = zeros(3,3,'single');
                height = single(0);
                if ~isempty(coder.target)               
                    corr = coder.ceval('getCorrection_s');
                    coder.ceval('copyHUnrot_s',coder.wref(H_unrot));
                    height = coder.ceval('getHeight_s');
                end
            end
        end
        
        function releaseImpl(obj)

        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 0;
        end
        
        function num = getNumOutputsImpl(~)
            num = 3;
        end
        
        function varargout = getOutputSizeImpl(~)
            varargout{1} = 1;
            varargout{2} = [3,3];
            varargout{3} = 1;
        end
        
        function varargout = getOutputDataTypeImpl(obj)
            if obj.dtype == DataTypeEnum.double_precision
                varargout{1} = 'double';
                varargout{2} = 'double';
                varargout{3} = 'double';
            else
                varargout{1} = 'single';
                varargout{2} = 'single';
                varargout{3} = 'single';
            end
        end
        
        function varargout = isOutputSizeLockedImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
        end
        
        function varargout = isOutputFixedSizeImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
        end
        
        function varargout = isOutputComplexImpl(~,~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
        end
        
        function varargout = isOutputComplexityLockedImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
        end
        
        
%         function validatePropertiesImpl(obj)
%         end
        
        function icon = getIconImpl(obj)
            % Define a string as the icon for the System block in Simulink.
            icon = sprintf('Loader');            
        end

        function varargout = getOutputNamesImpl(obj)
            varargout{1} = 'correction';
            varargout{2} = 'H_unrot';
            varargout{3} = 'height';
        end
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', Loader.getDescriptiveName());
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Loader';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
        
        function updateBuildInfo(buildInfo, context)
            if context.isCodeGenTarget('rtw')
                % Update buildInfo
                srcDir = fullfile(fileparts(mfilename('fullpath')),'src'); 
                includeDir = fullfile(fileparts(mfilename('fullpath')),'include');
                addIncludePaths(buildInfo,includeDir);
                % Use the following API's to add include files, sources and
                % linker flags
                addSourceFiles(buildInfo,'loader.c',srcDir);
                %addCompileFlags(buildInfo,{'-D_DEBUG=1'});
                %addDefines(buildInfo,'MY_DEFINE_1')
            end
        end
    end
end
