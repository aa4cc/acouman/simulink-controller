#include <stdio.h>
#include "loader.h"

int main(int argc, char const *argv[]){	
	char corrFName[50], HunrotFName[50], hFName[50], dtype[10];
	dtype[0] = 0;
	while (dtype[0] != 's' && dtype[0] != 'd') {
		printf("Enter data type (s=single / d=double): ");
		scanf("%s", dtype);
	}
	printf("Enter the name of correction file: ");
	scanf("%s", corrFName);
	printf("Enter the name of H_unrot file: ");
	scanf("%s", HunrotFName);
	printf("Enter the name of height file: ");
	scanf("%s", hFName);
	if (dtype[0] == 'd') {
		loadUp_double(corrFName, HunrotFName, hFName);
		double correction = getCorrection_d();
		double H_unrot[9];
		copyHUnrot_d(H_unrot);
		double height = getHeight_d();

		printf("Correction is %.3f\n", correction);
		printf("%% H_unrot = \n %% [ ");
		for (int i = 0; i < 3; ++i) {
			if (i>0) printf("%% ");
			for (int j = 0; j < 3; ++j) {
				printf("%.3e ", H_unrot[j*3+i]);
			}
			if (i < 2)
				printf(";\n");
			else
				printf("];\n");
		}
		printf("Height is %.1f\n", height);
	} else {
		loadUp_single(corrFName, HunrotFName, hFName);
		float correction = getCorrection_s();
		float H_unrot[9];
		copyHUnrot_s(H_unrot);
		float height = getHeight_s();

		printf("Correction is %.3f\n", correction);
		printf("%% H_unrot = \n %% [ ");
		for (int i = 0; i < 3; ++i) {
			if (i>0) printf("%% ");
			for (int j = 0; j < 3; ++j) {
				printf("%.3e ", H_unrot[j*3+i]);
			}
			if (i < 2)
				printf(";\n");
			else
				printf("];\n");
		}
		printf("Height is %.1f\n", height);
	}

	return 0;
}