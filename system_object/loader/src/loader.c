#include <stdio.h>
#include <string.h>
#include "loader.h"

double correction_d;
double H_unrot_d[9];
double height_d;

float correction_s;
float H_unrot_s[9];
float height_s;

void loadUp_double(const char* corrFName, const char* HUnrotFName, const char* heightFName) {
	FILE *f;
	if ((f = fopen(corrFName, "r")) != NULL) {
		fread(&correction_d, sizeof(double), 1, f);
		fclose(f);
	} else {
		fprintf(stderr, "Could not open file %s.\n", corrFName);
	}
	if ((f = fopen(HUnrotFName, "r")) != NULL) {
		fread(H_unrot_d, sizeof(double), 9, f);
		fclose(f);
	} else {
		fprintf(stderr, "Could not open file %s.\n", HUnrotFName);
	}
	if ((f = fopen(heightFName, "r")) != NULL) {
		fread(&height_d, sizeof(double), 1, f);
		fclose(f);
	} else {
		fprintf(stderr, "Could not open file %s.\n", heightFName);
	}
}

void loadUp_single(const char* corrFName, const char* HUnrotFName, const char* heightFName) {
	FILE *f;
	if ((f = fopen(corrFName, "r")) != NULL) {
		fread(&correction_s, sizeof(float), 1, f);
		fclose(f);
	} else {
		fprintf(stderr, "Could not open file %s.\n", corrFName);
	}
	if ((f = fopen(HUnrotFName, "r")) != NULL) {
		fread(H_unrot_s, sizeof(float), 9, f);
		fclose(f);
	} else {
		fprintf(stderr, "Could not open file %s.\n", HUnrotFName);
	}
	if ((f = fopen(heightFName, "r")) != NULL) {
		fread(&height_s, sizeof(float), 1, f);
		fclose(f);
	} else {
		fprintf(stderr, "Could not open file %s.\n", heightFName);
	}
}

double getCorrection_d() {
	return correction_d;
}

float getCorrection_s() {
	return correction_s;
}

void copyHUnrot_d(double *destination) {
	memcpy(destination, H_unrot_d, sizeof(double)*9);
}

void copyHUnrot_s(float *destination) {
	memcpy(destination, H_unrot_s, sizeof(float)*9);
}

double getHeight_d() {
	return height_d;
}

float getHeight_s() {
	return height_s;
}
