classdef DataTypeEnum < int32
    enumeration
        single_precision (1)
        double_precision (2)
    end
end