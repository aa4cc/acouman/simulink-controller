classdef GeneratorLargeArray < matlab.System ...
        & coder.ExternalDependency ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % This block sends settings to multiple generators at once. If the "Enable individual channels" option is on, you can send a 256x1 logical array to enable/disable individual outputs. If the option is off, you can only enable/disable the whole array.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties
        % Public, tunable properties.
    end
    
    properties (Nontunable)
        % Port0 1st device port
        Port0 = '/dev/ttyUSB0';
        % Port1 2nd device port
        Port1 = '/dev/ttyUSB1';
        % Port2 3rd device port
        Port2 = '/dev/ttyUSB2';
        % Port3 4th device port
        Port3 = '/dev/ttyUSB3';
    end
    
    properties (Nontunable, PositiveInteger)
        % Baud Baud rate
        Baud = 230400;
        % NumGen Number of generators
        NumGen = 1;
    end    
    
    properties (Nontunable, Logical)
        % IndividualEnable Enable individual channels
        IndividualEnable = false;
        % ShowAttempts Output the number of communication attempts
        ShowAttempts = false;
        % Master0 Master
        Master0 = true;
        % Master1 Master
        Master1 = false;
        % Master2 Master
        Master2 = false;
        % Master3 Master
        Master3 = false;
    end
    
    properties (Access=private)
        FileDescriptors (4,1);
    end
    
    properties (Access=private, Logical)
        PrevEnabled (1,1) = false;
    end
    
    properties (Hidden)
        DefaultEnable;
        DefaultDisable;
    end
    
    methods
        % Constructor
        function obj = GeneratorLargeArray(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj) 
            if isempty(coder.target)
                % Place simulation setup code here
            else
                obj.DefaultEnable = ones(obj.NumGen*64, 1, 'uint8');
                obj.DefaultDisable = zeros(obj.NumGen*64, 1, 'uint8');
                coder.cinclude('genControl.h');
                % Call C-function implementing device initialization
                coder.ceval('trigger_init');
                obj.FileDescriptors = int32([0;0;0;0]);
                obj.FileDescriptors(1) = coder.ceval('generator_open', [obj.Port0 0], int32(obj.Baud));
                if obj.NumGen > 1
                    obj.FileDescriptors(2) = coder.ceval('generator_open', [obj.Port1 0], int32(obj.Baud));
                    if obj.NumGen > 2
                        obj.FileDescriptors(3) = coder.ceval('generator_open', [obj.Port2 0], int32(obj.Baud));
                        if obj.NumGen > 3
                            obj.FileDescriptors(4) = coder.ceval('generator_open', [obj.Port3 0], int32(obj.Baud));
                        end
                    end
                end
                fd = obj.FileDescriptors(1:obj.NumGen);
                if obj.Master0
                    coder.ceval('generator_set_master',obj.FileDescriptors(1),int32(-1));
                    coder.ceval('generator_synchronize',obj.FileDescriptors(1),int32(-1));
                elseif obj.Master1
                    coder.ceval('generator_set_master',obj.FileDescriptors(2),int32(-1));
                    coder.ceval('generator_synchronize',obj.FileDescriptors(2),int32(-1));
                elseif obj.Master2
                    coder.ceval('generator_set_master',obj.FileDescriptors(3),int32(-1));
                    coder.ceval('generator_synchronize',obj.FileDescriptors(3),int32(-1));
                elseif obj.Master3
                    coder.ceval('generator_set_master',obj.FileDescriptors(4),int32(-1));
                    coder.ceval('generator_synchronize',obj.FileDescriptors(4),int32(-1));
                end
                coder.ceval('generators_send_enables', coder.rref(fd), int32(obj.NumGen), ...
                    coder.rref(obj.DefaultDisable), int32(-1), uint8(1), int32(0));
                %coder.ceval('trigger_low');
            end
        end
        
        function [en_attempts, phase_attempts] = stepImpl(obj,phases,arg2)  
            if isempty(coder.target)
                if arg2
                    acousticPressureSlice(phases,16,[-0.03,0.03],151,[-0.03,0.03],151,0.06,1);
                end
            else
                %coder.ceval('trigger_high');
                fd = obj.FileDescriptors(1:obj.NumGen);
                en_attempts = zeros(obj.NumGen, 1, 'int32');
                phase_attempts = zeros(obj.NumGen, 1, 'int32');
                if obj.IndividualEnable
                    coder.ceval('generators_send_enables', coder.rref(fd), int32(obj.NumGen), coder.rref(arg2), ...
                        int32(1), uint8(0), coder.wref(en_attempts));
                else
                    if arg2 ~= obj.PrevEnabled
                        obj.PrevEnabled = arg2;
                        if arg2
                            coder.ceval('generators_send_enables', coder.rref(fd), ...
                                int32(obj.NumGen), coder.rref(obj.DefaultEnable), int32(1), uint8(0), coder.wref(en_attempts));
                        else
                            coder.ceval('generators_send_enables', coder.rref(fd), ...
                                int32(obj.NumGen), coder.rref(obj.DefaultDisable), int32(1), uint8(0), coder.wref(en_attempts));
                        end
                    end
                end
                coder.ceval('generators_send_phases', coder.rref(fd), int32(obj.NumGen), ...
                    coder.rref(phases), int32(1), uint8(1), coder.wref(phase_attempts));
                %coder.ceval('trigger_low');
            end
        end
        
        function releaseImpl(obj)
            if isempty(coder.target)
                % Place simulation termination code here
            else
                % Call C-function implementing device termination
                %coder.ceval('trigger_high');
                fd = obj.FileDescriptors(1:obj.NumGen);
                coder.ceval('generators_send_enables', coder.rref(fd), int32(obj.NumGen), ...
                    coder.rref(obj.DefaultDisable), int32(-1), uint8(1), int32(0));
                for i = 1:obj.NumGen
                    coder.ceval('generator_close',obj.FileDescriptors(i));
                end
                %coder.ceval('trigger_low');
            end
        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 2;
        end
        
        function num = getNumOutputsImpl(obj)
            if obj.ShowAttempts
                num = 2;
            else
                num = 0;
            end
        end
        
        function flag = isInputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isInputFixedSizeImpl(~,~)
            varargout{1} = true;
        end
        
        function flag = isInputComplexityLockedImpl(~,~)
            flag = true;
        end
        
        function validateInputsImpl(obj, in1, in2)
            if isempty(coder.target)
                % Run input validation only in Simulation                
                validateattributes(in1,{'numeric'},{'size',[256,1]});
                if obj.IndividualEnable
                    validateattributes(in2,{'uint8'},{'size',[256,1]});
                else
                    validateattributes(in2,{'logical'},{'scalar'});
                end
            end
        end
        
        function validatePropertiesImpl(obj)
            validateattributes(obj.NumGen, {'numeric'}, {'scalar','<=',4,'>=',1});
            validateattributes(obj.Port0, {'char'}, {'row'});
            validateattributes(obj.Port1, {'char'}, {'row'});
            validateattributes(obj.Port2, {'char'}, {'row'});
            validateattributes(obj.Port3, {'char'}, {'row'});
        end

        function flag = isInactivePropertyImpl(obj,prop)
            % Return false if property is visible based on object 
            % configuration, for the command line and System block dialog
            flag = false;
            if obj.NumGen < 4
                if strcmp(prop, 'Port3') || strcmp(prop, 'Master3')
                    flag = true;
                end
                if obj.NumGen < 3
                    if strcmp(prop, 'Port2') || strcmp(prop, 'Master2')
                        flag = true;
                    end
                    if obj.NumGen < 2
                        if strcmp(prop, 'Port1') || strcmp(prop, 'Master1')
                            flag = true;
                        end
                    end
                end
            end
        end
        
        function icon = getIconImpl(obj)
            % Define a string as the icon for the System block in Simulink.
            if ~isempty(obj.Baud)
                icon = sprintf('%d channel generator\nBaudrate: %d', obj.NumGen*64, obj.Baud);
            else
                icon = sprintf('%d channel generator', obj.NumGen*64);
            end
            
        end

        function [name,name2] = getInputNamesImpl(obj)
            % Return input port names for System block
            name = 'Phases';
            if obj.IndividualEnable
                name2 = 'Channel enables';
            else
                name2 = 'On/Off';
            end
        end

        function [name1,name2] = getOutputNamesImpl(~)
            % Return output port names for System block
            name1 = sprintf('Enable\nattempts');
            name2 = sprintf('Phases\nattempts');
        end

        function [out,out2] = getOutputSizeImpl(obj)
            % Return size for each output port
            out = [obj.NumGen 1];
            out2 = [obj.NumGen 1];
        end

        function [out,out2] = getOutputDataTypeImpl(~)
            % Return data type for each output port
            out = "int32";
            out2 = "int32";
        end

        function [out,out2] = isOutputComplexImpl(~)
            % Return true for each output port with complex data
            out = false;
            out2 = false;
        end

        function [out,out2] = isOutputFixedSizeImpl(~)
            % Return true for each output port with fixed size
            out = true;
            out2 = true;
        end
        
        
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', NewGenerator.getDescriptiveName());
        end

        function groups = getPropertyGroupsImpl
            % Define property section(s) for System block dialog
            general_param = matlab.system.display.Section('Title','General settings','PropertyList',{'Baud','NumGen'});
            generators_settings = [matlab.system.display.Section('Title','First generator','PropertyList',{'Port0','Master0'}),...
                matlab.system.display.Section('Title','Second generator','PropertyList',{'Port1','Master1'}),...
                matlab.system.display.Section('Title','Third generator','PropertyList',{'Port2','Master2'}),...
                matlab.system.display.Section('Title','Fourth generator','PropertyList',{'Port3','Master3'})];
            output_settings = matlab.system.display.Section('Title','Output settings','PropertyList',{'IndividualEnable','ShowAttempts'});
            groups = [general_param,generators_settings,output_settings];
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = '256 channel generator control';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
        
        function updateBuildInfo(buildInfo, context)
            if context.isCodeGenTarget('rtw')
                % Update buildInfo
                srcDir = fullfile(fileparts(mfilename('fullpath')),'src'); 
                includeDir = fullfile(fileparts(mfilename('fullpath')),'include');
                addIncludePaths(buildInfo,includeDir);
                % Use the following API's to add include files, sources and
                % linker flags
                addIncludeFiles(buildInfo,{'genControl.h','crc8.h','bcm2835.h'},includeDir);
                addSourceFiles(buildInfo,{'genControl.c','crc8.c','bcm2835.c'},srcDir);
                addLinkFlags(buildInfo,{'-lrt','-lm'});
                %addLinkObjects(buildInfo,'sourcelib.a',srcDir);
                %addCompileFlags(buildInfo,{'-D_DEBUG=1'});
                %addDefines(buildInfo,'MY_DEFINE_1')
            end
        end
    end
end
