#include "genControl.h"

const uint8_t CRC_TABLE[CRC8_TABLE_SIZE] = {0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15, 0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d, 0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65, 0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d, 0xe0, 0xe7, 0xee, 0xe9, 0xfc, 0xfb, 0xf2, 0xf5, 0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd, 0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85, 0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd, 0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2, 0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea, 0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2, 0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a, 0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32, 0x1f, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0d, 0x0a, 0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42, 0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a, 0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c, 0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4, 0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec, 0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4, 0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c, 0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44, 0x19, 0x1e, 0x17, 0x10, 0x05, 0x02, 0x0b, 0x0c, 0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34, 0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b, 0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63, 0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b, 0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13, 0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb, 0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83, 0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb, 0xe6, 0xe1, 0xe8, 0xef, 0xfa, 0xfd, 0xf4, 0xf3};

const uint8_t MSG_ENABLE_ALL[74] = {0x02, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x5A, 0x2D, 0x16, 0x8B, 0x45, 0xA2, 0xD1, 0x68, 0xB4, 0x1F};

const uint8_t MSG_DISABLE_ALL[74] = {0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0D};

#define MOD_2PI(x)	(x>=0 ? fmod(x,2*M_PI) : 2*M_PI + fmod(x,2*M_PI))
#define RAD2DEG(x) (x * 180.0 / M_PI)

void print_hex(uint8_t *data, int length) {
	int i;
	for (i = 0; i < length; i++) {
		printf("0x%02x ", data[i]);
	}
}

int generator_open(const char *port, int baud) {
    printf("Opening port %s at %dbd\n", port, baud);
	int fp = open(port, O_EXCL | O_RDWR);
	printf("Port has file descriptor %d\n", fp);

	struct termios tio;
	int ret;
	if((ret=ioctl(fp, TCGETS, &tio))==-1){
		fprintf(stderr, "Getting termios structure failed. (%d)\n", ret);
		return -1;
	}

    tio.c_cflag &= ~PARENB;
    tio.c_cflag &= ~CBAUD;
	//tio.c_cflag |= BOTHER;
	tio.c_lflag &= ~ICANON;
	tio.c_lflag &= ~ISIG;
	tio.c_lflag &= ~IEXTEN;
	tio.c_lflag &= ~ECHO;
	tio.c_lflag &= ~ECHOE;
	tio.c_lflag &= ~ECHOK;
	tio.c_lflag &= ~ECHOCTL;
	tio.c_lflag &= ~ECHOKE;
	tio.c_iflag &= ~ICRNL;
	tio.c_iflag &= ~IXON;
	tio.c_oflag &= ~OPOST;
	tio.c_oflag &= ~ONLCR;

	tio.c_cc[VTIME] = 1;
	tio.c_cc[VMIN] = 0;
	switch (baud) {
		case 230400:
			cfsetispeed(&tio, B230400);
			cfsetospeed(&tio, B230400);
			break;
		default:
			fprintf(stderr, "Baudrate %d is not supported\n", baud);
	}

	if((ret = ioctl(fp, TCSETS, &tio))==-1){
		fprintf(stderr, "Setting termios structure failed. (%d)\n", ret);
		return -1;
	}

	return fp;
}

void trigger_init() {
	printf("Initializing BCM\n");
	if (!bcm2835_init()) fprintf(stderr, "Failed to init BCM2835\n");
	printf("Setting trigger\n");
	bcm2835_gpio_fsel(GPIO_TRIGGER, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_write(GPIO_TRIGGER, HIGH);
}

int generator_close(int fp){
	tcflush(fp, TCIOFLUSH);
    return close(fp); 
}

int generators_send_data(int *fp, int num_gen, uint8_t *data, int length, int tryouts, int *num_attempts) {
	uint8_t ack_received[MAX_GENERATORS];
	uint8_t ack_all = 0;
	uint8_t reply;
	memset(ack_received, 0, MAX_GENERATORS);
	int i;
	int tryouts_left = tryouts;
	if (num_attempts != NULL) {
		for (i = 0; i < num_gen; i++)
			num_attempts[i] = tryouts;
	}
	while (tryouts_left != 0) {
		--tryouts_left;
		for (i = 0; i < num_gen; i++) {
			if (!ack_received[i]) {
				tcflush(fp[i], TCIFLUSH);
				/*printf("Going to send: "); //debug
				print_hex(data + length*i, length);
				printf("\n");*/
				write(fp[i], data + length*i, length);
			}
		}
		if (tryouts_left != 0) {
			ack_all = 1;
			for (i = 0; i < num_gen; i++) {
				if (!ack_received[i]) {
					tcdrain(fp[i]);
					read(fp[i], &reply, 1);
					//printf("Received 0x%02x\n", reply); //debug
					ack_received[i] = (reply & MASK_SUCCESS) == MASK_SUCCESS;
					if (num_attempts != NULL && ack_received[i]) {
						num_attempts[i] = tryouts - tryouts_left;
					}
					ack_all &= ack_received[i];
				}
			}
			if (ack_all) {
				return 0;
			}
		}
	}
	return -1;
}

int generators_send_enables(int *fp, int num_gen, uint8_t *enables, int tryouts, uint8_t pulse, int *num_attempts) {
	/* Generate messages */
	uint8_t messages[74*MAX_GENERATORS];
	uint16_t duties[8];
	int i, j, k;
	for (i = 0; i < num_gen; i++) {
		messages[74*i] = CODE_DUTIES;
		for (j = 0; j < 8; j++) {
			for (k = 0; k < 8; k++)
				duties[k] = enables[64*i + 8*j + k] * DUTY_ENABLED;
			write_octet(duties, messages + 74*i + 9*j + 1);
		}
		messages[74*i + 73] = crc8(CRC_TABLE, messages + 74*i, 73, 0);
	}

	/* Communication */
	k = generators_send_data(fp, num_gen, messages, 74, tryouts, num_attempts); 
	if (pulse) {
		for (i = 0; i < num_gen; i++)
			tcdrain(fp[i]);
		trigger_pulse();
	}

	return k;
}

int generators_send_phases(int *fp, int num_gen, float *phases, int tryouts, uint8_t pulse, int *num_attempts) {
	/* Generate messages */
	uint8_t messages[74*MAX_GENERATORS];
	uint16_t phases_deg[8];
	int i, j, k;
	for (i = 0; i < num_gen; i++) {
		messages[74*i] = CODE_PHASES;
		for (j = 0; j < 8; j++) {
			for (k = 0; k < 8; k++)
				phases_deg[k] = (uint16_t) RAD2DEG(MOD_2PI(phases[64*i + 8*j + k]));
			write_octet(phases_deg, messages + 74*i + 9*j + 1);
		}
		messages[74*i + 73] = crc8(CRC_TABLE, messages + 74*i, 73, 0);
	}

	/* Communication */
	k = generators_send_data(fp, num_gen, messages, 74, tryouts, num_attempts); 
	if (pulse) {
		for (i = 0; i < num_gen; i++)
			tcdrain(fp[i]);
		trigger_pulse();
	}

	return k;
}

int generator_set_master(int fp, int tryouts) {
	uint8_t message[2];
	int num_attempts;
	message[0] = CODE_SET_MASTER;
	message[1] = crc8(CRC_TABLE, message, 1, 0);
	generators_send_data(&fp, 1, message, 2, tryouts, &num_attempts);
	return num_attempts;
}

int generator_set_slave(int fp, int tryouts) {
	uint8_t message[2];
	int num_attempts;
	message[0] = CODE_SET_SLAVE;
	message[1] = crc8(CRC_TABLE, message, 1, 0);
	generators_send_data(&fp, 1, message, 2, tryouts, &num_attempts);
	return num_attempts;
}

int generator_synchronize(int fp, int tryouts) {
	uint8_t message[2];
	int num_attempts;
	message[0] = CODE_SYNC;
	message[1] = crc8(CRC_TABLE, message, 1, 0);
	generators_send_data(&fp, 1, message, 2, tryouts, &num_attempts);
	return num_attempts;
}

void write_octet(uint16_t *data, uint8_t *dest) {
	dest[0] = data[0]>>1;
	int i;
	for (i = 1; i < 8; i++) {
		dest[i] = (data[i-1]<<(8-i)) | (data[i]>>(i+1));
	}
	dest[8] = data[7];
}

void trigger_pulse() {
	bcm2835_gpio_write(GPIO_TRIGGER, LOW);
	bcm2835_delayMicroseconds(TRIGGER_PULSE_LENGTH);
	bcm2835_gpio_write(GPIO_TRIGGER, HIGH);
}

void trigger_low() {
	bcm2835_gpio_write(GPIO_TRIGGER, LOW);
}

void trigger_high() {
	bcm2835_gpio_write(GPIO_TRIGGER, HIGH);
}