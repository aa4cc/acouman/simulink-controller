
#demonstration of Shifted Signal Generator communication
#Created 04.08.2018


import serial;
import time;

#master 10
#port = serial.Serial('COM20', 230400, parity= serial.PARITY_EVEN) #pozn.: paritu prvni verze generatoru v FPGA nekontroluje, paritni bit jen precte a zahodi (aktualni k 8.4.2018)

#port = serial.Serial('COM8', 115200, parity= serial.PARITY_EVEN) #pozn.: paritu prvni verze generatoru v FPGA nekontroluje, paritni bit jen precte a zahodi (aktualni k 8.4.2018)


#dataArray = [0,180,1,180,42,90,250,180] #set first channel phase to 0, first channel duty to 180, second phase to 1 etc. See screenCap files
dataArray = []
for i in range(64):
    dataArray.append(0);
    dataArray.append(180);
    #dataArray.append(180);
    #dataArray.append(180);


#dataArray = [180,180,1,180,42,90,250,180] #set first channel phase to 0, first channel duty to 180, second phase to 1 etc. See screenCap files

#dataArray = [0,140,0,160,0,180,0,300]

openCode = [255,255,240] #on receiving openCode, generator will shift further non-code bytes into its settings register
closeCode = [255,255,241] #on receiving stopCode, generator will ignore further non-code bytes. It will also copy data from settings register to individual channels.
 

dataStream = ""

for number in dataArray:
    ninebit = '{0:09b}'.format(number)      #generator uses 9 bit numbers to set phase and duty!!
    dataStream += ninebit
    #print("+ ", ninebit)

print(dataStream)
bytesArray = [dataStream[i:i+8] for i in range(0, len(dataStream), 8)]
print(bytesArray);
last = bytesArray.pop()
while len(last)%8 != 0: #appended zeroes will be shifted out of the end of the settings register
    last += "0";
bytesArray.append(last)
print(bytesArray)
bytesArray = bytesArray[::-1] #send in reverse order!!

intsArray = [int(i, 2) for i in bytesArray] 

#intsArray = [1]*5

theSum = 0
for num in intsArray:
    theSum += num;

theSum = theSum

print("the sum is:", theSum, " = ", theSum.to_bytes(2, byteorder = 'big'))

toSendArray = openCode + intsArray + closeCode
#toSendArray = intsArray 
#toSendArray = intsArray + closeCode
print("Sending channel settings : ", dataArray)
print("Actually sending bytes   : ",toSendArray)

print("gonna send")

#while(1):
 #   port.write(bytes(toSendArray) + theSum.to_bytes(2, byteorder = 'big'))
#    input()
    
input()

for num in toSendArray:
    #port.write(bytes(num))
    print("sending ",num)
    input()

