#ifndef GENERATOR_CONTROL_H
#define GENERATOR_CONTROL_H

//#include "rtwtypes.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>

#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <math.h>
#include <string.h>

#include "bcm2835.h"
#include "crc8.h"

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @defgroup code_macros Definitions of communication codes
 * Defines initial bytes that determine the type of message that is sent to
 * the generator.
 * @{
 */
/** Sets phase delays **/
#define CODE_PHASES             1
/** Sets duty cycles **/
#define CODE_DUTIES             2
/** Asks if the generator is a master **/
#define CODE_INQUIRE_MASTER     8
/** Requests synchronization **/
#define CODE_SYNC               16
/** Configures the generator as a master **/
#define CODE_SET_MASTER         32
/** Configures the generator as a slave **/
#define CODE_SET_SLAVE          64
/** @} **/

/** Duty cycle for enabled outputs (180 == 50%) **/
#define DUTY_ENABLED            180

/** Maximum number of connected generators **/
#define MAX_GENERATORS          4

/** Bit mask of a "successful transmission" reply. When a message is delivered
 * without errors, the genrator responds with a byte with four upper bits set
 * to 1. Otherwise, the four upper bits are set to 0. **/
#define MASK_SUCCESS            0xf0U

/**
 * @defgroup gpio_trigger GPIO trigger settings
 * Configures the GPIO trigger. The trigger is assigned to one of the GPIO 
 * ports on the Raspberry Pi and is set to high as default. When triggered, it
 * sends a low pulse, which is a signal for the FPGA generators to send the
 * received phase and duty cycle settings to their outputs.
 * @{
 */ 
/** GPIO port to which the trigger is assigned. The number of the port is
 * according to the wiringPi assignments **/
#define GPIO_TRIGGER            RPI_GPIO_P1_11
/** Length of the trigger pulse in microseconds. The delay uses the `usleep`
 * function, therefore, this is the minimal pulse length **/
#define TRIGGER_PULSE_LENGTH    25
/** @} **/

/**
 * @brief Opens and configures generator communication port.
 * 
 * Current port settings are
 *  - No parity
 *  - No echo
 *  - Non canonical
 * 
 * @param port Name of the serial port
 * @param baud Baudrate
 * @return int Port file descriptor
 */
int generator_open(const char *port, int baud);

/**
 * @brief Initializes the GPIO trigger
 * 
 */
void trigger_init();

/**
 * @brief Closes the generator communication port.
 * 
 * Flushes the buffer and closes the communication port.
 * 
 * @param fp File descriptor
 * @return int Return value of the close function
 */
int generator_close(int fp);

/**
 * @brief Writes eight ninebit numbers into nine bytes of memory.
 * 
 * @param data Eight ninebit numbers (assumes that each entry is <= 0x01ff)
 * @param dest Nine bytes to which the numbers should be written
 */
void write_octet(uint16_t *data, uint8_t *dest);

/**
 * @brief Sends data packets to multiple generators at once.
 * 
 * @param fp Array of file descriptors
 * @param num_gen Number of generators
 * @param data Pointer to data. Should be an array of size `length*num_gen`
 * @param length Length of one message (assuming that all messages have the 
 *  same length)
 * @param tryouts Number of attempts. The messages are repeatedly sent to each
 *  generator until either an acknowledgement arrives, or the number of 
 *  attempts is exceeded. Set to -1 for infinite number of attempts.
 * @param num_attempts If not NULL, the function will write, how many times was
 *  the message sent to each generator, to the specified address.
 * @return int 0 if received all acknowledgements, -1 otherwise.
 */
int generators_send_data(int *fp, int num_gen, uint8_t *data, int length, int tryouts, int *num_attempts);

/**
 * @brief Send "enable channels" message to multiple generators at once.
 * 
 * Allows the user to enable/disable individual outputs on the generators
 * by setting the duty cycle of channels to 0 or 50%.
 * 
 * @param fp Array of file descriptors
 * @param num_gen Number of generators
 * @param enables Channel enables. Should be an array of size `64*num_gen` with
 *  values either 0 (disable) or 1 (enable).
 * @param tryouts Number of attempts. See @ref generators_send_data.
 * @param num_attempts See @ref generators_send_data.
 * @param pulse If true (`>0`), a trigger pulse is sent to the generators after
 *  the last message is sent. If false (`==0`) no pulse is sent -- in this 
 *  configuration, the function doesn't wait until the last message is 
 *  transmitted.
 * @return int 0 if received all acknowledgements, -1 otherwise.
 */
int generators_send_enables(int *fp, int num_gen, uint8_t *enables, int tryouts, uint8_t pulse, int *num_attempts);

/**
 * @brief Sends phase delays to multiple generators at once.
 * 
 * @param fp Array of file descriptors
 * @param num_gen Number of generators
 * @param phases Array of phase delays in radians. The delays don't need to be
 * in the \f$\left[ 0, 2\pi \right)\f$ range, as the function automatically
 * converts them.
 * @param tryouts Number of attempts. See @ref generators_send_data.
 * @param num_attempts See @ref generators_send_data.
 * @param pulse If true (`>0`), a trigger pulse is sent to the generators after
 *  the last message is sent. If false (`==0`) no pulse is sent -- in this 
 *  configuration, the function doesn't wait until the last message is 
 *  transmitted.
 * @return int 0 if received all acknowledgements, -1 otherwise.
 */
int generators_send_phases(int *fp, int num_gen, float *phases, int tryouts, uint8_t pulse, int *num_attempts);

/**
 * @brief Sends a "set master" message to a selected generator.
 * 
 * @param fp File descriptor
 * @param tryouts Number of attempts (see @ref generators_send_data).
 * @return int How many times was the message sent.
 */
int generator_set_master(int fp, int tryouts);

/**
 * @brief Sends a "set slave" message to a selected generator.
 * 
 * @param fp File descriptor
 * @param tryouts Number of attempts (see @ref generators_send_data).
 * @return int How many times was the message sent.
 */
int generator_set_slave(int fp, int tryouts);

/**
 * @brief Sends a "synchronize" message to a selected generator.
 * 
 * The sync message must be sent to the master, otherwise it is ignored.
 * 
 * @param fp File descriptor
 * @param tryouts Number of attempts (see @ref generators_send_data).
 * @return int How many times was the message sent.
 */
int generator_synchronize(int fp, int tryouts);

/**
 * @brief Sends the trigger pulse.
 * 
 */
void trigger_pulse();

/**
 * @brief Sets the trigger signal to low.
 * 
 */
void trigger_low();

/**
 * @brief Sets the trigger signal to high.
 * 
 */
void trigger_high();

#ifdef __cplusplus
}
#endif

#endif