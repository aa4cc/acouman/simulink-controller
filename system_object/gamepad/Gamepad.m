classdef Gamepad < matlab.System ...
        & coder.ExternalDependency ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % Use this block to process input form one or more gamepads.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties
        % Public, tunable properties.
    end
    
    properties (Nontunable)
        % Path1 First gamepad
        Path1 = '/dev/input/event0';
        % Path2 Second gamepad
        Path2 = '/dev/input/event1';
    end
    
    properties (Nontunable, PositiveInteger)
        % Num Number of gamepads
        Num = 2;
    end
    
    properties (Nontunable, Logical)
        % ShowAxes Output joystick axes
        ShowAxes = true;
        % ShowAB Output A and B button
        ShowAB = true;
        % ShowXY Output X and Y button
        ShowXY = true;
        % ShowLR Output left and right button
        ShowLR = true;
        % ShowSS Output select and start button
        ShowSS = true;
    end
    
    methods
        % Constructor
        function obj = Gamepad(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
        
        function oo = get_output_offsets(obj)
            oo = zeros(1,10);
            out_count = 0;
            if obj.ShowAxes
                oo(1) = 1;
                oo(2) = 2;
                out_count = out_count + 2;
            end
            if obj.ShowAB
                oo(3) = out_count + 1;
                oo(4) = out_count + 2;
                out_count = out_count + 2;
            end
            if obj.ShowXY
                oo(5) = out_count + 1;
                oo(6) = out_count + 2;
                out_count = out_count + 2;
            end
            if obj.ShowLR
                oo(7) = out_count + 1;
                oo(8) = out_count + 2;
                out_count = out_count + 2;
            end
            if obj.ShowSS
                oo(9) = out_count + 1;
                oo(10) = out_count + 2;
            end
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj) 
            %obj.output_offsets = obj.get_output_offsets();
            if ~isempty(coder.target)
                coder.cinclude('gamepad.h');
                % Call C-function implementing device initialization
                if ~isempty(obj.Path1)
                    coder.ceval('init_gamepad', int32(0), [obj.Path1 0]);
                end
                if ~isempty(obj.Path2) && obj.Num > 1
                    coder.ceval('init_gamepad', int32(1), [obj.Path2 0]);
                end
            end
        end
        
        function varargout = stepImpl(obj)
            num_out = obj.getNumOutputsImpl / obj.Num;
            for k = int32(0:obj.Num-1)
                out_count = int32(0);
                if obj.ShowAxes
                    varargout{num_out*k+1} =  int8(0);
                    varargout{num_out*k+2} =  int8(0);
                    out_count = out_count + 2;
                end
                if obj.ShowAB
                    varargout{num_out*k+out_count+1} =  uint8(0);
                    varargout{num_out*k+out_count+2} =  uint8(0);
                    out_count = out_count + 2;
                end
                if obj.ShowXY
                    varargout{num_out*k+out_count+1} =  uint8(0);
                    varargout{num_out*k+out_count+2} =  uint8(0);
                    out_count = out_count + 2;
                end
                if obj.ShowLR
                    varargout{num_out*k+out_count+1} =  uint8(0);
                    varargout{num_out*k+out_count+2} =  uint8(0);
                    out_count = out_count + 2;
                end
                if obj.ShowSS
                    varargout{num_out*k+out_count+1} =  uint8(0);
                    varargout{num_out*k+out_count+2} =  uint8(0);
                end
                if ~isempty(coder.target)
                    out_count = int32(0);
                    coder.ceval('update_gamepad', k);
                    if obj.ShowAxes
                        varargout{num_out*k+1} = coder.ceval('get_x_axis', k);
                        varargout{num_out*k+2} = coder.ceval('get_y_axis', k);
                        out_count = out_count + 2;
                    end
                    if obj.ShowAB
                        varargout{num_out*k+out_count+1} = coder.ceval('get_a_button', k);
                        varargout{num_out*k+out_count+2} = coder.ceval('get_b_button', k);
                        out_count = out_count + 2;
                    end
                    if obj.ShowXY
                        varargout{num_out*k+out_count+1} = coder.ceval('get_x_button', k);
                        varargout{num_out*k+out_count+2} = coder.ceval('get_y_button', k);
                        out_count = out_count + 2;
                    end
                    if obj.ShowLR
                        varargout{num_out*k+out_count+1} = coder.ceval('get_left_button', k);
                        varargout{num_out*k+out_count+2} = coder.ceval('get_right_button', k);
                        out_count = out_count + 2;
                    end
                    if obj.ShowSS
                        varargout{num_out*k+out_count+1} = coder.ceval('get_select_button', k);
                        varargout{num_out*k+out_count+2} = coder.ceval('get_start_button', k);
                    end
                end
            end
        end
        
        function releaseImpl(obj)
            if isempty(coder.target)
                % Place simulation termination code here
            else
                % Call C-function implementing device termination
                for k = int32(0:obj.Num-1)
                    coder.ceval('close_gamepad', k);
                end
                %coder.ceval('sink_terminate');
            end
        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 0;
        end
        
        function num = getNumOutputsImpl(obj)
            num = (obj.ShowAB + obj.ShowAxes + obj.ShowLR + obj.ShowSS + obj.ShowXY)*2*obj.Num;
        end
        
        function varargout = isOutputSizeLockedImpl(obj,~)
            varargout = cell(1,obj.getNumOutputsImpl);
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = true;
            end
        end
        
        function varargout = isOutputFixedSizeImpl(obj,~)
            varargout = cell(1,obj.getNumOutputsImpl);
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = true;
            end
        end
        
        function varargout = isOutputComplexImpl(obj,~)
            varargout = cell(1,obj.getNumOutputsImpl);
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = false;
            end
        end
        
        function varargout = isOutputComplexityLockedImpl(obj,~)
            varargout = cell(1,obj.getNumOutputsImpl);
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = true;
            end
        end
        
        function validatePropertiesImpl(~)
        end
        
        function icon = getIconImpl(~)
            % Define a string as the icon for the System block in Simulink.
            icon = sprintf('SNES USB\ngamepad');           
        end

        function varargout = getOutputNamesImpl(obj)
            % Return input port names for System block
            varargout = cell(1,obj.getNumOutputsImpl);
            num_out = obj.getNumOutputsImpl / obj.Num;
            oo = obj.get_output_offsets();
            for k = int32(0:obj.Num-1)
                if obj.ShowAxes
                    varargout{num_out*k+oo(1)} = 'X axis';
                    varargout{num_out*k+oo(2)} = 'Y axis';
                end
                if obj.ShowAB
                    varargout{num_out*k+oo(3)} = 'A button';
                    varargout{num_out*k+oo(4)} = 'B button';
                end
                if obj.ShowXY
                    varargout{num_out*k+oo(5)} = 'X button';
                    varargout{num_out*k+oo(6)} = 'Y button';
                end
                if obj.ShowLR
                    varargout{num_out*k+oo(7)} = 'Left button';
                    varargout{num_out*k+oo(8)} = 'Right button';
                end
                if obj.ShowSS
                    varargout{num_out*k+oo(9)} = 'Select button';
                    varargout{num_out*k+oo(10)} = 'Start button';
                end
            end
        end

        function varargout = getOutputSizeImpl(obj)
            % Return size for each output port
            varargout = cell(1,10*obj.Num);
            for k = 1:obj.getNumOutputsImpl
                varargout{k} = [1 1];
            end
        end

        function varargout = getOutputDataTypeImpl(obj)
            % Return data type for each output port
            varargout = cell(1,obj.getNumOutputsImpl);
            num_out = obj.getNumOutputsImpl / obj.Num;
            oo = obj.get_output_offsets();
            for k = int32(0:obj.Num-1)
                if obj.ShowAxes
                    varargout{num_out*k+oo(1)} = 'int8';
                    varargout{num_out*k+oo(2)} = 'int8';
                end
                if obj.ShowAB
                    varargout{num_out*k+oo(3)} = 'uint8';
                    varargout{num_out*k+oo(4)} = 'uint8';
                end
                if obj.ShowXY
                    varargout{num_out*k+oo(5)} = 'uint8';
                    varargout{num_out*k+oo(6)} = 'uint8';
                end
                if obj.ShowLR
                    varargout{num_out*k+oo(7)} = 'uint8';
                    varargout{num_out*k+oo(8)} = 'uint8';
                end
                if obj.ShowSS
                    varargout{num_out*k+oo(9)} = 'uint8';
                    varargout{num_out*k+oo(10)} = 'uint8';
                end
            end
        end       
        
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', Gamepad.getDescriptiveName());
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'SNES USB gamepad';
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
        
        function updateBuildInfo(buildInfo, context)
            if context.isCodeGenTarget('rtw')
                % Update buildInfo
                srcDir = fullfile(fileparts(mfilename('fullpath')),'src'); 
                includeDir = fullfile(fileparts(mfilename('fullpath')),'include');
                addIncludePaths(buildInfo,includeDir);
                % Use the following API's to add include files, sources and
                % linker flags
                addIncludeFiles(buildInfo,'gamepad.h',includeDir);
                addSourceFiles(buildInfo,'gamepad.c',srcDir);
                addLinkFlags(buildInfo,{'-levdev'});
                %addLinkObjects(buildInfo,'sourcelib.a',srcDir);
                %addCompileFlags(buildInfo,{'-D_DEBUG=1'});
                %addDefines(buildInfo,'MY_DEFINE_1')
            end
        end
    end
end
