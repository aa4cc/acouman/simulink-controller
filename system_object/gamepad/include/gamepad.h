#ifndef GAMEPAD_H
#define GAMEPAD_H

#include <stdint.h>

/** Maximum number of gamepads */
#define MAX_NUM_GAMEPADS 2

/** 
 * @defgroup code_macros Definitions of event codes
 * Defines event codes for buttons and joystick
 * @{
 */
/** Message type for joystick */
#define TYPE_JOYSTICK   3
/** Message type for buttons */
#define TYPE_BUTTON     1
/** Code for joystick x-axis */
#define CODE_JOY_X      0
/** Code for joystick y-axis */
#define CODE_JOY_Y      1
/** Code for A button */
#define CODE_A          289
/** Code for B button */
#define CODE_B          290
/** Code for X button */
#define CODE_X          288
/** Code for Y button */
#define CODE_Y          291
/** Code for SELECT button */
#define CODE_SELECT     296
/** Code for START button */
#define CODE_START      297
/** Code for LEFT button */
#define CODE_LEFT       292
/** Code for RIGHT button */
#define CODE_RIGHT      293
/** @} **/

/** 
 * @defgroup bitmasks Bitmasks for button state
 * Bitmask used to determine button state
 * @{
 */
/** Mask for A button */
#define MASK_A          0b00000001U
#define SHIFT_A         0
/** Mask for B button */
#define MASK_B          0b00000010U
#define SHIFT_B         1
/** Mask for X button */
#define MASK_X          0b00000100U
#define SHIFT_X         2
/** Mask for Y button */
#define MASK_Y          0b00001000U
#define SHIFT_Y         3
/** Mask for SELECT button */
#define MASK_SELECT     0b00010000U
#define SHIFT_SELECT    4
/** Mask for START button */
#define MASK_START      0b00100000U
#define SHIFT_START     5
/** Mask for LEFT button */
#define MASK_LEFT       0b01000000U
#define SHIFT_LEFT      6
/** Mask for RIGHT button */
#define MASK_RIGHT      0b10000000U
#define SHIFT_RIGHT     7
/** @} **/

/** Structure representing the current status of a SNES gampepad */
struct gamepad_state
{
    /** Non-zero if the gamepad's file descriptor is valid and open. */
    uint8_t ready;
    /** Joystick states. */
    int8_t x_axis, y_axis;
    /** Button state. */
    uint8_t button_state;
};

/**
 * @brief Initializes a SNES USB gamepad.
 * 
 * @param id ID that will be later used to access the gamepad. ID must be an integer between 0 (inclusive) and MAX_NUM_GAMEPADS (exclusive)
 * @param path Path to gamepad (usually /dev/input/eventX)
 */
void init_gamepad(int id, const char *path);

/**
 * @brief Updates gamepad status
 * 
 * Reads the queued events and updates the gamepad status.
 * 
 * @param id Gamepad ID
 */
void update_gamepad(int id);

/**
 * @brief Terminates gamepad
 * 
 * Closes the file descriptor associated to the specified gamepad
 * 
 * @param id Gamepad ID
 */
void close_gamepad(int id);

/**
 * @brief Get the joystick x-axis
 * 
 * Returns the x-axis state as a signed integer 
 * 
 * @param id Gamepad ID
 * @return int8_t State (-1=left, 0=center, 1=right)
 */
int8_t get_x_axis(int id);

/**
 * @brief Get the joystick y-axis
 * 
 * Returns the y-axis state as a signed integer 
 * 
 * @param id Gamepad ID
 * @return int8_t State (-1=up, 0=center, 1=down)
 */
int8_t get_y_axis(int id);

/**
 * @brief Get the A button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_a_button(int id);

/**
 * @brief Get the B button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_b_button(int id);

/**
 * @brief Get the X button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_x_button(int id);

/**
 * @brief Get the Y button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_y_button(int id);

/**
 * @brief Get the SELECT button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_select_button(int id);

/**
 * @brief Get the START button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_start_button(int id);

/**
 * @brief Get the LEFT button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_left_button(int id);

/**
 * @brief Get the RIGHT button state
 * 
 * @param id Gamepad ID
 * @return uint8_t State (0=released, 1=pressed)
 */
uint8_t get_right_button(int id);

#endif