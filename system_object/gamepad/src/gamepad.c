#include "gamepad.h"
#include <libevdev/libevdev.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

struct libevdev *devices[MAX_NUM_GAMEPADS];
int fds[MAX_NUM_GAMEPADS];
struct gamepad_state status[MAX_NUM_GAMEPADS];

void init_gamepad(int id, const char *path) {
    printf("Opening gamepad %d at %s\n", id, path);
    fds[id] = open(path, O_RDONLY|O_NONBLOCK);
    int rc = libevdev_new_from_fd(fds[id], &devices[id]);
    memset(&status[id], 0, sizeof(struct gamepad_state));
    if (rc < 0) {
        fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
        return;
    }
    status[id].ready = 1;
    printf("Input device name: \"%s\"\n", libevdev_get_name(devices[id]));
}

void close_gamepad(int id) {
    libevdev_free(devices[id]);
    close(fds[id]);
    status[id].ready = 0;
}

void update_gamepad(int id) {
    int rc;
    struct input_event ev;
    if (status[id].ready) {
    do {        
        rc = libevdev_next_event(devices[id], LIBEVDEV_READ_FLAG_NORMAL, &ev);
        if (rc == 0) {
            if (ev.type == TYPE_JOYSTICK) {
                switch (ev.code) {
                case CODE_JOY_X:
                    status[id].x_axis = (int8_t) (ev.value >> 7) + ((ev.value >> 6) & 1) - 1;
                    break;
                case CODE_JOY_Y:
                    status[id].y_axis = (int8_t) (ev.value >> 7) + ((ev.value >> 6) & 1) - 1;
                    break;
                default:
                    fprintf(stderr, "Undefined joystick code %d.\n", ev.code);
                }
            } else if (ev.type == TYPE_BUTTON) {
                switch (ev.code) {
                case CODE_A:
                    if (ev.value)
                        status[id].button_state |= MASK_A;
                    else
                        status[id].button_state &= ~MASK_A;
                    break;
                case CODE_B:
                    if (ev.value)
                        status[id].button_state |= MASK_B;
                    else
                        status[id].button_state &= ~MASK_B;
                    break;
                case CODE_X:
                    if (ev.value)
                        status[id].button_state |= MASK_X;
                    else
                        status[id].button_state &= ~MASK_X;
                    break;
                case CODE_Y:
                    if (ev.value)
                        status[id].button_state |= MASK_Y;
                    else
                        status[id].button_state &= ~MASK_Y;
                    break;
                case CODE_START:
                    if (ev.value)
                        status[id].button_state |= MASK_START;
                    else
                        status[id].button_state &= ~MASK_START;
                    break;
                case CODE_SELECT:
                    if (ev.value)
                        status[id].button_state |= MASK_SELECT;
                    else
                        status[id].button_state &= ~MASK_SELECT;
                    break;
                case CODE_LEFT:
                    if (ev.value)
                        status[id].button_state |= MASK_LEFT;
                    else
                        status[id].button_state &= ~MASK_LEFT;
                    break;
                case CODE_RIGHT:
                    if (ev.value)
                        status[id].button_state |= MASK_RIGHT;
                    else
                        status[id].button_state &= ~MASK_RIGHT;
                    break;
                default:
                    fprintf(stderr, "Undefined button code %d.\n", ev.code);
                }
            }
        }
    } while (rc == 1 || rc == 0);
    }
}

int8_t get_x_axis(int id) {
    return status[id].x_axis;
}

int8_t get_y_axis(int id) {
    return status[id].y_axis;
}

uint8_t get_a_button(int id) {
    return (status[id].button_state & MASK_A) >> SHIFT_A;
}

uint8_t get_b_button(int id) {
    return (status[id].button_state & MASK_B) >> SHIFT_B;
}

uint8_t get_x_button(int id) {
    return (status[id].button_state & MASK_X) >> SHIFT_X;
}

uint8_t get_y_button(int id) {
    return (status[id].button_state & MASK_Y) >> SHIFT_Y;
}

uint8_t get_start_button(int id) {
    return (status[id].button_state & MASK_START) >> SHIFT_START;
}

uint8_t get_select_button(int id) {
    return (status[id].button_state & MASK_SELECT) >> SHIFT_SELECT;
}

uint8_t get_left_button(int id) {
    return (status[id].button_state & MASK_LEFT) >> SHIFT_LEFT;
}

uint8_t get_right_button(int id) {
    return (status[id].button_state & MASK_RIGHT) >> SHIFT_RIGHT;
}
