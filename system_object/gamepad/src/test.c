#include <stdio.h>
#include "gamepad.h"
#include <unistd.h>
#include <stdint.h>

void print_gamepad(int id) {
    int8_t x = get_x_axis(id);
    int8_t y = get_y_axis(id);
    if (x == 0 && y == 0)
        printf("o");
    if (x == -1)
        printf("<");
    if (y == -1)
        printf("˄");
    if (y == 1)
        printf("˅");  
    if (x == 1)
        printf(">");
    printf(" ");
    if (get_select_button(id))
        printf("SELECT ");
    if (get_start_button(id))
        printf("START ");
    if (get_a_button(id))
        printf("A "); 
    if (get_b_button(id))
        printf("B "); 
    if (get_x_button(id))
        printf("X ");
    if (get_y_button(id))
        printf("Y ");
    if (get_left_button(id))
        printf("L ");
    if (get_right_button(id))
        printf("R ");
    printf("\n");
}

int main() {
    init_gamepad(0, "/dev/input/event0");

    while (1) {
        usleep(1000000);
        update_gamepad(0);
        print_gamepad(0);
    }
}