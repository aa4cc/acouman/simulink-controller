classdef Pong < matlab.System ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % A system for processing pong inputs.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties (Nontunable)
        % AXIS_MULT Joystick axis multiplier
        axis_mult single = - 1.5;
        % PADDLE_X Paddle x-position
        paddle_x single = 35;
        % Y_MAX Maximum paddle y-position
        y_max single = 25;
        % PADDLE_WIDTH Paddle width (used in collision checking)
        paddle_width single = 8;
        % PADDLE_HEIGHT Paddle height (used in collision checking)
        paddle_height single = 25;
        % BALL_R Ball radius (used in collision checking)
        ball_r single = 5;
        % X_BOUND Ball boundary (x-axis)
        x_bound single = 40;
        % Y_BOUND Ball boundary (y-axis)
        y_bound single = 35;
        % BALL_SPEED Ball speed
        ball_speed single = 10;
        % TS Sampling time
        Ts single = 0.04;
    end
    
    properties (Access = private)
        left_paddle = struct('x',single(0),'y',single(0));
        right_paddle = struct('x',single(0),'y',single(0));
        ball = struct('x',single(0),'y',single(0),'v_x',single(0),'v_y',single(0));
        pong_status int32 = int32(0); % pong status 0=waiting to serve, 1=playing
    end
    
    methods
        % Constructor
        function obj = Pong(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)
        
        function reset_ball(obj)
           obj.ball.x = single(0);
           obj.ball.y = single(0);
           ball_angle = (rand(1, 'single') - single(0.5)) * single(pi/2); % random ball angle between +-45 degrees
           obj.ball.v_y = obj.ball_speed * sin(ball_angle);
           if rand(1, 'single') < 0.5 % randomly choose left or right
               obj.ball.v_x = obj.ball_speed * cos(ball_angle);
           else
               obj.ball.v_x = - obj.ball_speed * cos(ball_angle);
           end
        end
        
        function check_paddle_bounce(obj, paddle_y, ball_y)
            y_gap = obj.paddle_height/2 + obj.ball_r;
            if (ball_y < paddle_y + y_gap) && (ball_y > paddle_y - y_gap)
                obj.ball.v_x = - obj.ball.v_x;
            end
        end

        function setupImpl(obj)
            obj.left_paddle.x = obj.paddle_x;
            obj.left_paddle.y = single(0);
            obj.right_paddle.x = - obj.paddle_x;
            obj.right_paddle.y = single(0);
            obj.reset_ball();
        end
        
        function [LPx, LPy, LPr, RPx, RPy, RPr, Bx, By] = stepImpl(obj, axis1, axis2, serve)  
            obj.left_paddle.y = min(max(obj.left_paddle.y + obj.axis_mult*single(axis1), -obj.y_max), obj.y_max);
            obj.right_paddle.y = min(max(obj.right_paddle.y + obj.axis_mult*single(axis2), -obj.y_max), obj.y_max);
            
            LPx = [single(0); obj.left_paddle.x];
            LPy = [single(0); obj.left_paddle.y];
            LPr = [single(0); single(pi/2)];
            RPx = [single(0); obj.right_paddle.x];
            RPy = [single(0); obj.right_paddle.y];
            RPr = [single(0); single(pi/2)];
            
            if obj.pong_status == int32(0)
                Bx = [single(0); obj.ball.x];
                By = [single(0); obj.ball.y];
                if serve
                    obj.pong_status = int32(1);
                end
            else
                obj.ball.x = obj.ball.x + obj.ball.v_x * obj.Ts;
                obj.ball.y = obj.ball.y + obj.ball.v_y * obj.Ts;
                
                x_gap = obj.paddle_width/2 + obj.ball_r;
                if (obj.ball.x < - obj.paddle_x + x_gap) && (obj.ball.v_x < 0)
                    obj.check_paddle_bounce(obj.ball.y, obj.right_paddle.y);
                    if obj.ball.x < - obj.x_bound
                        obj.reset_ball();
                        obj.pong_status = int32(0);
                    end
                end
                if (obj.ball.x > obj.paddle_x - x_gap) && (obj.ball.v_x > 0)
                    obj.check_paddle_bounce(obj.ball.y, obj.left_paddle.y);
                    if obj.ball.x > obj.x_bound
                        obj.reset_ball();
                        obj.pong_status = int32(0);                        
                    end
                end
                
                if (obj.ball.y > obj.y_bound - obj.ball_r && obj.ball.v_y > 0) || (obj.ball.y < - obj.y_bound + obj.ball_r && obj.ball.v_y < 0)
                    obj.ball.v_y = - obj.ball.v_y;
                end
                
                Bx = [obj.ball.v_x; obj.ball.x];
                By = [obj.ball.v_y; obj.ball.y];
            end
        end
        
        function releaseImpl(obj)

        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 3;
        end
        
        function num = getNumOutputsImpl(~)
            num = 8;
        end
        
        function varargout = getOutputSizeImpl(~)
            varargout{1} = [2 1];
            varargout{2} = [2 1];
            varargout{3} = [2 1];
            varargout{4} = [2 1];
            varargout{5} = [2 1];
            varargout{6} = [2 1];
            varargout{7} = [2 1];
            varargout{8} = [2 1];
        end
        
        function varargout = getOutputDataTypeImpl(~)
            varargout{1} = 'single';
            varargout{2} = 'single';
            varargout{3} = 'single';
            varargout{4} = 'single';
            varargout{5} = 'single';
            varargout{6} = 'single';
            varargout{7} = 'single';
            varargout{8} = 'single';
        end
        
        function varargout = isOutputSizeLockedImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            varargout{4} = true;
            varargout{5} = true;
            varargout{6} = true;
            varargout{7} = true;
            varargout{8} = true;
        end
        
        function varargout = isOutputFixedSizeImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            varargout{4} = true;
            varargout{5} = true;
            varargout{6} = true;
            varargout{7} = true;
            varargout{8} = true;
        end
        
        function varargout = isOutputComplexImpl(~,~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
            varargout{4} = false;
            varargout{5} = false;
            varargout{6} = false;
            varargout{7} = false;
            varargout{8} = false;
        end
        
        function varargout = isOutputComplexityLockedImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            varargout{4} = true;
            varargout{5} = true;
            varargout{6} = true;
            varargout{7} = true;
            varargout{8} = true;
        end
        
        
%         function validatePropertiesImpl(obj)
%         end
        
        function icon = getIconImpl(obj)
            % Define a string as the icon for the System block in Simulink.
            icon = sprintf('Pong');            
        end

        function varargout = getOutputNamesImpl(obj)
            varargout{1} = sprintf('Left paddle\nx-reference');
            varargout{2} = sprintf('Left paddle\ny-reference');
            varargout{3} = sprintf('Left paddle\nrotation-ref');
            varargout{4} = sprintf('Right paddle\nx-reference');
            varargout{5} = sprintf('Right paddle\ny-reference');
            varargout{6} = sprintf('Right paddle\nrotation-ref');
            varargout{7} = sprintf('Ball\nx-reference');
            varargout{8} = sprintf('Ball\ny-reference');
        end
        
        function varargout = getInputNamesImpl(obj)
            varargout{1} = 'Joystick 1';
            varargout{2} = 'Joystick 2';
            varargout{3} = 'Serve';
        end
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', Pong.getDescriptiveName());
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Code generation';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Pong';
        end
    end
end
