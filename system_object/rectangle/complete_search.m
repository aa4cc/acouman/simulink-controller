function [k,p,o] = complete_search(Fx, Fy, T, forces, torques, p_max, w_T)
    n_points = int32(size(forces, 2));
    
    k = int32(0);
    p = single(0);
    o = single(inf);
    
    for i = int32(1):n_points
        press_x = min(max(Fx/forces(1,i), single(0)), p_max);
        press_y = min(max(Fy/forces(2,i), single(0)), p_max);
        press_T = min(max(T/torques(i), single(0)), p_max);
        press = (press_x + press_y + w_T*press_T) / (2 + w_T);
        o_i = (Fx - forces(1,i) .* press).^2 + ...
            (Fy - forces(2,i) .* press).^2 + w_T^2 * (T - torques(i) .* press).^2;
        if o_i < o
            o = o_i;
            k = i;
            p = press;
        end
    end
end
