classdef F2PRectangle < matlab.System ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % Converts requested force and torque into high-pressure point.
    %
    % This template includes most, but not all, possible properties,
    % attributes, and methods that you can implement for a System object in
    % Simulink.
    %
    %#codegen
    properties (Nontunable)
        % METHOD Optimization method
        method (1,1) OptMethod
        % SAT Control saturation
        sat single = 1.8;
        % W_T Torque weight in optimization criterion
        w_T single = 0.1;
    end
    
    properties (Access = private)
        forces
        torques
        point_x
        point_y
    end
    
    methods
        % Constructor
        function obj = F2PRectangle(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods (Access=protected)

        function setupImpl(obj)
            S = coder.load('full_volume.mat');
            obj.point_x = single(S.X);
            obj.point_y = single(S.Y);
            obj.forces = single(S.F);
            obj.torques = single(S.T);
        end
        
        function [P_x, P_y, P, Fx_actual, Fy_actual, T_actual] = stepImpl(obj, x, y, rot, Fx, Fy, T)  
            rot = pmpi_2(rot);
            c = cos(rot);
            s = sin(rot);
            
            Fx_t = Fx*c + Fy*s;
            Fy_t = Fy*c - Fx*s;
            
            switch obj.method
                case OptMethod.complete_search
                    [k, P] = complete_search(Fx_t, Fy_t, T, obj.forces, obj.torques, obj.sat, obj.w_T);
                case OptMethod.PSO
                    [k, P] = pso(Fx_t, Fy_t, T, obj.forces, obj.torques, obj.sat, obj.w_T);
            end
            
            P_x_t = obj.point_x(k);
            P_y_t = obj.point_y(k);
            Fx_rt = obj.forces(1, k) * P;
            Fy_rt = obj.forces(2, k) * P;
            T_actual = obj.torques(k) * P;
            
            P_x = (x + P_x_t*c - P_y_t*s) * 1e-3;
            P_y = (y + P_y_t*c + P_x_t*s) * 1e-3;
            Fx_actual = Fx_rt*c - Fy_rt*s;
            Fy_actual = Fy_rt*c + Fx_rt*s;
            
            P = P * 1e3 + 700;            
        end
        
        function releaseImpl(obj)

        end
    end
    
    methods (Access=protected)
        %% Define input properties
        function num = getNumInputsImpl(~)
            num = 6;
        end
        
        function num = getNumOutputsImpl(~)
            num = 6;
        end
        
        function varargout = getOutputSizeImpl(~)
            varargout{1} = 1;
            varargout{2} = 1;
            varargout{3} = 1;
            varargout{4} = 1;
            varargout{5} = 1;
            varargout{6} = 1;
        end
        
        function varargout = getOutputDataTypeImpl(~)
            varargout{1} = 'single';
            varargout{2} = 'single';
            varargout{3} = 'single';
            varargout{4} = 'single';
            varargout{5} = 'single';
            varargout{6} = 'single';
        end
        
        function varargout = isOutputSizeLockedImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            varargout{4} = true;
            varargout{5} = true;
            varargout{6} = true;
        end
        
        function varargout = isOutputFixedSizeImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            varargout{4} = true;
            varargout{5} = true;
            varargout{6} = true;
        end
        
        function varargout = isOutputComplexImpl(~,~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
            varargout{4} = false;
            varargout{5} = false;
            varargout{6} = false;
        end
        
        function varargout = isOutputComplexityLockedImpl(~,~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
        end
        
        
%         function validatePropertiesImpl(obj)
%         end
        
        function icon = getIconImpl(obj)
            % Define a string as the icon for the System block in Simulink.
            icon = sprintf('Force to pressure\nRectangle');            
        end

        function varargout = getOutputNamesImpl(obj)
            varargout{1} = 'Point x';
            varargout{2} = 'Point y';
            varargout{3} = 'Amplitude';
            varargout{4} = 'F_x actual';
            varargout{5} = 'F_y actual';
            varargout{6} = 'Torque actual';
        end
        
        function varargout = getInputNamesImpl(obj)
            varargout{1} = 'Object x';
            varargout{2} = 'Object y';
            varargout{3} = 'Object rotation';
            varargout{4} = 'F_x required';
            varargout{5} = 'F_y required';
            varargout{6} = 'Torque required';
        end
    end
    
    methods (Static, Access=protected)
        function header = getHeaderImpl
            % Define header panel for System block dialog
           header = matlab.system.display.Header(mfilename('class'), 'Title', F2PRectangle.getDescriptiveName());
        end

        function simMode = getSimulateUsingImpl(~)
            simMode = 'Code generation';
        end

        function flag = showSimulateUsingImpl
            % Return false if simulation mode hidden in System block dialog
            flag = false;
        end
    end
    
    methods (Static)
        function name = getDescriptiveName()
            name = 'Force to pressure on Rectangle';
        end
    end
end
