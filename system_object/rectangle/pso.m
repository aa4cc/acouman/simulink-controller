function [k,p,o] = pso(Fx, Fy, T, forces, torques, p_max, w_T)
    n_iter = int32(10);
    n_part = int32(5);
    n_points = int32(size(forces, 2));
    w_start = single(0.9);
    w_end = single(0.4);
    
    w = w_start;
    w_r = (w_end / w_start) ^ (1/single(n_iter));
    
    c1 = single(1.5);
    c2 = single(1);
    
    K = randi(300, 1, n_part, 'int32');
    V_K = 20 * randn(1, n_part, 'single');
    P = zeros(1, n_part, 'single');
    for j = int32(1):n_part
        px = min(max(Fx/forces(1,K(j)), single(0)), p_max);
        py = min(max(Fy/forces(2,K(j)), single(0)), p_max);
        pr = min(max(T/torques(K(j)), single(0)), p_max);
        P(j) = (px + py + w_T*pr) / (2 + w_T);
    end
    
    crit = @(point, press) ((Fx - forces(1,point) .* press).^2 + ...
        (Fy - forces(2,point) .* press).^2 + w_T^2 * (T - torques(point) .* press).^2);
    
    K_g = K;
    P_g = P;
    O_g = crit(K_g, P_g);
    [o, arg] = min(O_g);
    k = K(arg);
    p = P(arg);
    
    for i = int32(1):n_iter        
        for j = int32(1):n_part
            r1 = rand(1,'single');
            r2 = rand(1,'single');
            V_K(j) = w*V_K(j) + c1*r1*single(K_g(j) - K(j)) + c2*r2*single(k - K(j));
            K(j) = mod(K(j) + int32(V_K(j)) - 1, n_points) + 1;
            px = min(max(Fx/forces(1,K(j)), single(0)), p_max);
            py = min(max(Fy/forces(2,K(j)), single(0)), p_max);
            pr = min(max(T/torques(K(j)), single(0)), p_max);
            P(j) = (px + py + w_T*pr) / (2 + w_T);
            o_j = crit(K(j), P(j));
            if o_j < O_g(j)
                O_g(j) = o_j;
                K_g(j) = K(j);
                P_g(j) = P(j);
                if o_j < o
                    o = o_j;
                    k = K(j);
                    p = P(j);
                end
            end
        end
        
        w = w * w_r;
    end
end
