%% Add folders to path
addpath('system_object/lm','system_object/newGenerator','system_object/loader',...
    'MATLAB_scripts','subsystem','system_object/raspi-ballpos/matlabSystemObject',...
    'system_object/trajectory','system_object/lqr','system_object/gamepad');
%% Load constants
load('controller_constants/floating_ball/f2p.mat');
load('controller_constants/floating_ball/state_space_grad.mat');
load('controller_constants/floating_ball/state_space_press.mat');
load('controller_constants/floating_ball/noise.mat');
load('controller_constants/calibration.mat');

run generate_lookup.m

%% Parameters
%%% Particle radius
Rp = 5;

%%% State feedback -- pressure-point control
% LQR
LQ = diag([1,10]);
LR = 1e-8;
[K_press,~,e_p] = dlqr(A_p,B_p,LQ,LR,[]);
% disp(e) % show closed loop poles

% LQI
LQ = diag([1,10,0.001]);
LR = 1e-8;
Kext_press = lqi(ss(A_p,B_p,C_p,D_p,0.02), LQ, LR, []);

%%% State feedback -- gradient control
% LQR
LQ = diag([1,10]);
LR = 1e-5;
[K_grad,~,e_g] = dlqr(A_g,B_g,LQ,LR,[]);
% disp(e) % show closed loop poles

% LQI
LQ = diag([1,10,0.001]);
LR = 1e-5;
Kext_grad = lqi(ss(A_g,B_g,C_g,D_g,0.02), LQ, LR, []);