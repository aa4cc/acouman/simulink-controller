# Simulink models

This document contains a description of the Simulink models and their usage.

## Using the simulink models

The models require the MATLAB system object from [raspi-ballpos](https://github.com/aa4cc/raspi-ballpos) to function.
This repository is added as a submodule.
Upon cloning, run ``git pull --recurse-submodules`` to initialize the repo.
In addition, some models require [Eigen](http://eigen.tuxfamily.org/) and [LBFGS++](http://yixuan.cos.name/LBFGSpp/) libraries on the target device.

### Floating ball

There are two models, `floatingBall_pressure_control.slx` and `floatingBall_gradient_control`, that implement manipulation of floating spherical objects.

### RGBballs

A model for manipulation of three floating spherical objects.

### Triangle(s)

A model for manipulation of one (and two) floating triangular object(s).

### Triangle formation

A model that assembles up to four triangular objects in a predefined formation.

### All in one

This model implements three models (focal points, ball on solid surface, floating ball) with a switching system. It is created to be used with the old version of the [web interface](https://gitlab.fel.cvut.cz/aa4cc/acouman/webinterface).

### Focal points

This is a simple demonstration model. It creates a high-pressure point at a specified position.

### Ball on solid surface

This model implements a controller for positioning a ball on a solid surface. If the controller fails to stabilize the ball, check the calibration of the platform.
