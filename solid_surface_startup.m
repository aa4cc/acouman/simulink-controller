%% Add folders to path
addpath('system_object/bfgs','system_object/newGenerator','MATLAB_scripts','system_object/raspi-ballpos/matlabSystemObject');
%% Load constants
load('controller_constants/solid_surface/completeModel.mat');
load('controller_constants/solid_surface/LQRcoefs.mat');
load('controller_constants/solid_surface/singleCoordinateModel.mat');
