function x_i = clamping_integrator(x, sat, reset)
% CLAMPING_INTEGRATOR Integrator with output clamping and external reset
%
% x_i = CLAMPING_INTEGRATOR(x, sat, reset) Returns cumulative sums of
% signal x multiplied by sampling time (0.02 s). Inputs sat and reset
% should be logic variables. If sat is true, the integrator limits its
% output. If reset is true, the integrator state is set to zero.
    persistent state
    if isempty(state)
        state = 0;
    end
    if reset
        state = 0;
    else
        if (~sat || state * x < 0)
            state = state + x;
        end
    end
    x_i = state;
end

