function x_new = k_steps(x_old, u, k, A, B)
%K_STEPS Performs k time-update steps on a linear system
%
% x_new = K_STEPS(x_old, u_new, k, A, B) Returns state vector x_new = x(t+k)
%of a linear discrete-time system x(t+1) = A x(t) + B u(t).
% Input arguments:
%  x_old = x(t)
%  u = u(t+k-1) -- previous inputs are kept in persistent memory
%  k -- number of steps
%  A -- state transition matrix
%  B -- input matrix
    persistent u_prev
    if isempty(u_prev)
        u_prev = zeros(numel(u), 9, 'single'); % static allocation -- maximum delay length is 10
    end
    
    x_new = x_old;
    for i = 1:k-1
        x_new = A*x_new + B*u_prev(:, i);
    end
    x_new = A*x_new + B*u;
    
    if k > 1
        u_prev(:, 1:k-2) = u_prev(:, 2:k-1);
        u_prev(:, k-1) = u;
    end    
end

