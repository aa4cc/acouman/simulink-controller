function [x_sat, y_sat, is_sat] = sat_2D(x, y, limit)
% SAT_2D Two dimensional saturation
%
% [x_sat, y_sat] = SAT_2D(x, y, limit) Limits the variables x and y, such
% that x^2 + y^2 <= limit^2.
%
% [x_sat, y_sat, is_sat] = SAT_2D(x, y, limit) The function can also return
% a logical variable which is true when x and y are saturated.
    A = sqrt(x^2 + y^2);
    A_ratio = A / limit;
    if A_ratio > 1
        x_sat = x / A_ratio;
        y_sat = y / A_ratio;
        if nargout == 3
            is_sat = true;
        end
    else
        x_sat = x;
        y_sat = y;
        if nargout == 3
            is_sat = false;
        end
    end
end
