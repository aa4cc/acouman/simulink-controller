%% Add folders to path
addpath('system_object/lm','system_object/newGenerator','MATLAB_scripts', ...
    'system_object/gamepad','subsystem','system_object/raspi-ballpos/matlabSystemObject',...
    'controller_constants/Triangle', 'system_object/trajectory', 'system_object/loader', ...
    'system_object/triangle');

%% Load constants
load controller_constants\calibration.mat

run generate_lookup
run full_volume_model

%% Controller constants
%%% MPC
MPC_Q_t = single([1 0;0 10]);
MPC_Q_r = single([1 0;0 10]*100);
MPC_R = single(1);
