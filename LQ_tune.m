%% models
m = 2.3e-4;
b = 4.44e-4;

Ac = [-b/m 0; 1 0];
Bc = [1/m; 0];
Cc = [0 1];
Dc = 0;
model = ss(Ac, Bc, Cc, Dc);

model_d = c2d(model, 0.02);
A = model_d.A;
B = model_d.B;
C = model_d.C;
D = model_d.D;

%% LQR
LQ = diag([1,10]);
LR = 1;
Kc = lqr(Ac,Bc,LQ,LR,[])

LQ = diag([1,10]);
LR = 1;
K = dlqr(A,B,LQ,LR,[])

%% LQI
LQ = diag([1,0,50]);
LR = 1;
Kcext = lqi(model, LQ, LR, 0)

LQ = diag([1,0,50]);
LR = 1;
Kext = lqi(model_d, LQ, LR, 0)