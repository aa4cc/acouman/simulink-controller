%% continuous-time model
%%% translational dynamics
A_tc = [-0.9904, 0; 1, 0];
B_tc = [9.1587; 0];
KQ_tc = 0.3186;
KR_t = 0.0873;
%%% rotational dynamics
A_rc = [-0.5300, 0; 1, 0];
B_rc = [0.6752; 0];
KQ_rc = 0.090;
KR_r = 0.001;

%% discretization
Ts = 0.04;
%%% translational dynamics
ABt = expm([A_tc, B_tc; 0 0 0] * Ts);
A_t = ABt(1:2,1:2);
B_t = ABt(1:2,3);
syms tau
KQ_t = int(expm(A_tc*(Ts-tau))*[1 0;0 1]*expm(A_tc*(Ts-tau))',tau,0,Ts)*KQ_tc;
KQ_t = double(KQ_t);
%%% rotational dynamics
ABr = expm([A_rc, B_rc; 0 0 0] * Ts);
A_r = ABr(1:2,1:2);
B_r = ABr(1:2,3);
KQ_r = int(expm(A_rc*(Ts-tau))*[1 0;0 1]*expm(A_rc*(Ts-tau))',tau,0,Ts)*KQ_rc;
KQ_r = double(KQ_r);

%% LQ controller
LQ_t = diag([1,10]);
LR_t = 100;
K_t = dlqr(A_t, B_t, LQ_t, LR_t, []);

LQ_r = diag([1,10]);
LR_r = 5e-4;
K_r = dlqr(A_r, B_r, LQ_r, LR_r, []);

%% Cleanup
clear A_tc B_tc A_rc B_rc KQ_tc KQ_rc ABt ABr tau 