%% continuous-time model
%%% translational dynamics
A_tc = [-0.4756, 0; 1, 0];
B_tc = [3.3160; 0];
KQ_tc = 2.4110;
KR_t_rect = 0.0873;
%%% rotational dynamics
A_rc = [-0.5081, 0; 1, 0];
B_rc = [0.0460; 0];
KQ_rc = 0.1313;
KR_r_rect = 0.001;

%% discretization
Ts = 0.04;
%%% translational dynamics
ABt = expm([A_tc, B_tc; 0 0 0] * Ts);
A_t_rect = ABt(1:2,1:2);
B_t_rect = ABt(1:2,3);
syms tau
KQ_t_rect = int(expm(A_tc*(Ts-tau))*[1 0;0 1]*expm(A_tc*(Ts-tau))',tau,0,Ts)*KQ_tc;
KQ_t_rect = double(KQ_t_rect);
%%% rotational dynamics
ABr = expm([A_rc, B_rc; 0 0 0] * Ts);
A_r_rect = ABr(1:2,1:2);
B_r_rect = ABr(1:2,3);
KQ_r_rect = int(expm(A_rc*(Ts-tau))*[1 0;0 1]*expm(A_rc*(Ts-tau))',tau,0,Ts)*KQ_rc;
KQ_r_rect = double(KQ_r_rect);

%% LQ controller
LQ_t = diag([1,10]);
LR_t = 40;
K_t_rect = dlqr(A_t_rect, B_t_rect, LQ_t, LR_t, []);

LQ_r = diag([1,10]);
LR_r = 8e-5;
K_r_rect = dlqr(A_r_rect, B_r_rect, LQ_r, LR_r, []);

%% Cleanup
clear A_tc B_tc A_rc B_rc KQ_tc KQ_rc ABt ABr tau 