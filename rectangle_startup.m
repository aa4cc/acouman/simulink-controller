%% Add folders to path
addpath('system_object/lm','system_object/newGenerator','MATLAB_scripts', ...
    'system_object/rectangle','subsystem','system_object/raspi-ballpos/matlabSystemObject',...
    'system_object/trajectory', 'system_object/loader');

%% Load constants
load controller_constants/calibration.mat

run generate_lookup
run controller_constants/Rectangle/full_volume_model
